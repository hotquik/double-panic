# Double Panic

Have you played classic video games? Sure... but can you play 2 at the same time?

[Play Preview Online](http://games.hotsoft.ninja/DoublePanic/)

![Double Panic preview](http://games.hotsoft.ninja/DoublePanic/Images/screenshot-1.png)
![Double Panic preview](http://games.hotsoft.ninja/DoublePanic/Images/screenshot-2.png)
Created with Unity and Maya

To do list:
- Bug: Tetris pieces go inside frames
- Feature: Improve Arkanoid graphics
- Feature: Add Audio effects and music
- Feature: Improve GUI
- Feature: Add scoreboard

Enrique González
enriquejgonzalezb@gmail.com
