﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    Text m_text;
    //TextMesh m_textMesh;
    int m_score;

    public int Score
    {
        get => m_score;
        set
        {
            if (m_score != value)
            {
                m_score = value;
                m_text.text = m_score.ToString();
                //m_textMesh.text = m_score.ToString();
            }
        }
    }

    void Awake()
    {
        m_text = GetComponent<Text>();
        m_text.text = m_score.ToString();
        //m_textMesh = GetComponent<TextMesh>();
        //m_textMesh.text = m_score.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
