﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    //Text m_text;
    ScoreText m_scoreText;

    public int Score { 
        get => m_scoreText.Score;
        set => m_scoreText.Score = value;
    }

    void Awake ()
    {
        m_scoreText = GetComponentInChildren<ScoreText>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
