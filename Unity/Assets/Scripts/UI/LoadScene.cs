﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class LoadScene : MonoBehaviour
{

    public string sceneName;



    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadTargetScene()
    {
        Util.Log("Loading " + sceneName, gameObject);
        SceneManager.LoadScene(sceneName);
    }

}
