﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MiniGameMessage : MonoBehaviour
{
    public delegate void MiniGameMessageCallback(MiniGameHandler miniGameHandler);


    public static float DefaultAnimationTime = 1;
    Text m_text;
    GameObject m_canvas;
    float m_timeUntilHide;
    MiniGameMessageCallback callback;
    MiniGameHandler miniGameHandler;

    void Awake ()
    {
        m_text = GetComponentInChildren<Text>();
        if (m_canvas == null)
            m_canvas = gameObject.transform.Find("Canvas").gameObject;
        m_canvas.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_canvas.activeSelf)
        {
            m_timeUntilHide -= Time.deltaTime;

            if (m_timeUntilHide <= 0)
            {
                m_canvas.SetActive(false);
                if (callback  != null)
                {
                    callback.Invoke(miniGameHandler);
                }
            }
        }
    }

    public void ShowMessage (string text, float animationTime = float.NaN, MiniGameMessageCallback callback = null, MiniGameHandler miniGameHandler = null)
    {
        if (float.IsNaN(animationTime))
            animationTime = DefaultAnimationTime;

        this.callback = callback;
        this.miniGameHandler = miniGameHandler;
        m_timeUntilHide = animationTime;
        m_text.text = text;
        m_canvas.SetActive(true);
    }
}
