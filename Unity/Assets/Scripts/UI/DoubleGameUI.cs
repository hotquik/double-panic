﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleGameUI : MonoBehaviour
{
    ScoreBoard leftScoreBoard, rightScoreBoard, totalScoreBoard, lifesBoard;
    GameObject gameOver, restartButton;
    MiniGameMessage leftMiniGameMessage, rightMiniGameMessage;

    
    public ScoreBoard LeftScoreBoard { get => leftScoreBoard; }
    public ScoreBoard RightScoreBoard { get => rightScoreBoard;  }
    public ScoreBoard TotalScoreBoard { get => totalScoreBoard; }
    public ScoreBoard LifesBoard { get => lifesBoard; }
    public GameObject GameOver { get => gameOver;  }
    public MiniGameMessage LeftMiniGameMessage { get => leftMiniGameMessage; }
    public MiniGameMessage RightMiniGameMessage { get => rightMiniGameMessage; }
    

    void Awake ()
    {
        leftScoreBoard = transform.Find("LeftScoreBoard").GetComponent<ScoreBoard>();
        Util.CheckIsNull(leftScoreBoard, "LeftScoreBoard", gameObject);
        rightScoreBoard = transform.Find("RightScoreBoard").GetComponent<ScoreBoard>();
        Util.CheckIsNull(rightScoreBoard, "RightScoreBoard", gameObject);
        totalScoreBoard = transform.Find("TotalScoreBoard").GetComponent<ScoreBoard>();
        Util.CheckIsNull(totalScoreBoard, "TotalScoreBoard", gameObject);
        lifesBoard = transform.Find("LifesBoard").GetComponent<ScoreBoard>();
        Util.CheckIsNull(lifesBoard, "LifesBoard", gameObject);
        gameOver = transform.Find("GameOverCanvas").gameObject;
        Util.CheckIsNull(gameOver, "GameOverCanvas", gameObject);
        restartButton = transform.Find("RestartButtonCanvas").gameObject;
        Util.CheckIsNull(restartButton, "RestartButtonCanvas", gameObject);
        leftMiniGameMessage = transform.Find("LeftMiniGameMessage").GetComponent<MiniGameMessage>();
        Util.CheckIsNull(leftMiniGameMessage, "LeftMiniGameMessage", gameObject);
        rightMiniGameMessage = transform.Find("RightMiniGameMessage").GetComponent<MiniGameMessage>();
        Util.CheckIsNull(rightMiniGameMessage, "RightMiniGameMessage", gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnGameOver()
    {
        gameOver.SetActive(true);
        restartButton.SetActive(true);
    }
}
