﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameController : MonoBehaviour
{
    public float GameSpeed = 1;

    public virtual void Awake()
    {
    }

    public virtual void Start()
    {
    }

    public virtual void Update()
    {
    }

    public virtual bool GetMiniGameInputDown(MiniGame minigame, MiniGameInput miniGameInput)
    {
        return false;
    }

    public virtual bool GetMiniGameInputUp(MiniGame minigame, MiniGameInput miniGameInput)
    {
        return false;
    }

    public virtual bool GetMiniGameInput(MiniGame minigame, MiniGameInput miniGameInput)
    {
        return false;
    }
}
