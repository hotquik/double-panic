﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum MiniGameInput {
    None = -1,
    Right = 0,
    Down = 1,
    Left = 2,
    Up = 3,    
}

public abstract class MiniGame : MonoBehaviour
{
    public class Cell
    {
        public int Column;
        public int Row;
        public Vector3 Origin;
        public Vector3 Center;

        public Cell ( int column, int row, Vector3 origin, Vector3 center)
        {
            Column = column;
            Row = row;
            Origin = origin;
            Center = center;
        }

        public override string ToString()
        {
            return "MiniGame.Cell: {Column: " + Column.ToString() + ", Row: " + Row.ToString() + ", Origin: " + Origin.ToString() + ", Center: " + Center.ToString() + " }";
        }
    }

    #region Static
    public static Vector3 Up = new Vector3(0, 1, 0);
    public static Vector3 Left = new Vector3(-1, 0, 0);
    public static Vector3 Down = new Vector3(0, -1, 0);
    public static Vector3 Right = new Vector3(1, 0, 0);
    public static Vector3 Front = new Vector3(0, 0, 1);
    public static Vector3 Back = new Vector3(0, 0, -1);

    public static float CellSize = 1;
    public static Vector3 CellCenterVector = new Vector3(0.5f * CellSize, 0.5f * CellSize, 0.5f * CellSize);
    
    public static Vector3 Clockwise = new Vector3(0, 0, -1);
    public static Vector3 CounterClockwise = new Vector3(0, 0, 1);
    #endregion Static

    #region Public
    public string GameName;
    public GameObject Frame;
    public GameObject Objects;
    public float GameSpeed = 1;
    public int pointsForVictory = 100;

    public int Rows { get => rows; set => rows = value; }
    public int Columns { get => columns; set => columns = value; }
    public Cell[][] Cells;
    public int LayerMask { get; set; }
    public int Score { get; set; }
    public bool GameStarted { get => gameStarted; set => gameStarted = value; }
    public bool GameOver { get => gameOver; set => gameOver = value; }
    public bool Victory { get => victory; set => victory = value; }
    public bool GamePlaying { get => gameStarted && !victory && !gameOver; }
    public GameController GameController { get => gameController; }
    #endregion Public

    #region Protected
    protected GameController gameController;

    protected int rows = 20;
    protected int columns = 20;
    protected bool gameStarted = false;
    protected bool gameOver = false;
    protected bool victory = false;
    #endregion Protected

    public virtual void Awake()
    {
        InitGrid();
    }

    public virtual void Start()
    {
    }

    void InitGrid()
    {
        #region Create grid Cells 
        Cells = new Cell[Columns][];
        for (int column = 0; column < Columns; column++)
        {
            Cells[column] = new Cell[Rows];
            for (int row = 0; row < Rows; row++)
            {
                Vector3 origin = CellOriginPosition(column, row);
                Vector3 center = CellCenterPosition(column, row);
                Cell cell = new Cell(column, row, origin, center);
                Cells[column][row] = cell;
            }
        }
        #endregion  Create grid Cells 
    }

    public Cell GetCell( int column, int row) 
    {
        if (column >=0 && column < Cells.Length && row >= 0 && row < Cells[column].Length)
        {
            return Cells[column][row];
        } 
        else
        {
            return null;
        }
    }

    public virtual void Config(GameController gameController,  float gameSpeed)
    {
        this.gameController = gameController;
        GameSpeed = gameSpeed;
        UpdateAnimatorsSpeed();
    }

    public virtual void StartGame()
    {
        gameStarted = true;
        UpdateAnimatorsState();
    }

    public GameObject InstantiateObject(GameObject prefab, Vector3 position)
    {
        return InstantiateObject(prefab, position, true);
    }

    public GameObject InstantiateObject(GameObject prefab, Vector3 position, bool positionIsLocal )
    {
        return InstantiateObject(prefab, position, Quaternion.identity, null, positionIsLocal);
    }

    public GameObject InstantiateObject(GameObject prefab, Vector3 position, Quaternion rotation, GameObject parent = null, bool positionIsLocal = true)
    {
        GameObject go = Instantiate(prefab, Vector3.zero, rotation);
        go.transform.SetParent(parent == null ? Objects.transform : parent.transform);
        if (positionIsLocal)
        {
            go.transform.localPosition = position;
        }
        else
        {
            go.transform.position = position;
        }
        return go;
    }

    public virtual void Update()
    {
        if (Score >= pointsForVictory)
        {
            OnVictory();
        }
    }

    public virtual void OnScorePoints(int points)
    {
        Score += points;
    }

    public virtual void OnGameOver()
    {
        gameOver = true;
        UpdateAnimatorsState();
    }

    public virtual void OnVictory()
    {
        victory = true;
        UpdateAnimatorsState();
    }

    public void UpdateAnimatorsSpeed()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        for (int i = 0; i < animators.Length; i++)
        {
            animators[i].speed = GameSpeed;
        }
    }

    public void UpdateAnimatorsState()
    {
        bool gamePlaying = GamePlaying;
        Animator[] animators = GetComponentsInChildren<Animator>();
        for (int i = 0; i < animators.Length; i++)
        {
            SetAnimatorBoolParameter(animators[i], "GamePlaying", gamePlaying);
        }
    }

    private void SetAnimatorBoolParameter(Animator animator, string paramName, bool value) {
        for (int i = 0; i < animator.parameters.Length; i++)
        {
            if (animator.parameters[i].name == paramName)
            {
                animator.SetBool(animator.parameters[i].nameHash, value);
            }
        }
    }

    public Vector3 CellOriginPosition(int column, int row)
    {
        return new Vector3(column * CellSize, row * CellSize, 0);
    }

    public Vector3 CellCenterPosition(int column, int row)
    {
        return CellOriginPosition(column, row) + CellCenterVector;
    }

    public RaycastHit[] SweepTestCell(int column, int row, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal, bool useCenter  = true)
    {
        Vector3 origin = ( useCenter ? CellCenterPosition(column, row) : CellOriginPosition(column, row) )
            + Objects.transform.position + Back;
        Vector3 direction = Front;
        RaycastHit[] hits = Physics.RaycastAll(origin, direction, 2 * CellSize, LayerMask, queryTriggerInteraction);
        return hits;
    }

    public static RaycastHit[] FindHitsByType(RaycastHit[] hits, Type type)
    {
        return Array.FindAll<RaycastHit>(hits, hit =>
        {
            GameObject otherGameObject = hit.transform.gameObject;
            System.Object o = otherGameObject.GetComponent(type);
            return (o != null);
        });
    }

    public static MiniGameInput OppositeInput(MiniGameInput miniGameInput)
    {
        MiniGameInput result = MiniGameInput.None;
        switch (miniGameInput)
        {
            case MiniGameInput.Up:
                result = MiniGameInput.Down;
                break;
            case MiniGameInput.Down:
                result = MiniGameInput.Up;
                break;
            case MiniGameInput.Left:
                result = MiniGameInput.Right;
                break;
            case MiniGameInput.Right:
                result = MiniGameInput.Left;
                break;
        }
        return result;
    }

}
