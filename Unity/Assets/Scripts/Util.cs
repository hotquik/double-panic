﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    static int logIndex = 0;

    public static void Log(string msg, GameObject gameObject = null)
    {
        logIndex++;
        Debug.Log((gameObject != null ? "{" + gameObject.name + "}" : "") + "[" + logIndex.ToString() + "] " + msg);
    }

    public static void LogVector(Vector3 vector3, string name = "", GameObject gameObject = null)
    {
        Log(name + "( x:" + vector3.x + ", y:" + vector3.y + ", z:" + vector3.z + " )", gameObject);
    }

    public static void CheckIsNull(object parameter, string parameterName, GameObject gameObject = null)
    {
        if (parameter == null)
        {
            Log(parameterName + " is null", gameObject);
        }
    }

    public static void LogColor(Color color, string name = "", GameObject gameObject = null)
    {
        Log(name + "( r:" + color.r + ", g:" + color.g + ", b:" + color.b + " )", gameObject);
    }

    public static Transform FindTransformByName(Transform parent, string name)
    {
        if (parent.name.Equals(name)) return parent;
        foreach (Transform child in parent)
        {
            Transform result = FindTransformByName(child, name);
            if (result != null) return result;
        }
        return null;
    }

    public static List<Transform> FindTransformsByTag(Transform parent, string tag, List<Transform> transforms = null)
    {
        if (transforms == null) {
            transforms = new List<Transform>();
        }
        if (parent.tag.Equals(tag)) transforms.Add(parent);
        foreach (Transform child in parent)
        {
            FindTransformsByTag(child, tag, transforms);
        }
        return transforms;
    }

}