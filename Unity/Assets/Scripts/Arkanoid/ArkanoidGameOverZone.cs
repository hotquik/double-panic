﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidGameOverZone : MonoBehaviour
{
    public ArkanoidGame ArkanoidGame { get; set; }
    
    void OnTriggerEnter(Collider other)
    {
        ArkanoidBall ball = other.gameObject.GetComponent<ArkanoidBall>();
        if (ball != null)
        {
            ArkanoidGame.OnGameOver();
        }
    }
}
