﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ArkanoidBase : KinematicObject
{
    float baseMoveMagnitude = 4;
    float adjustedBaseMoveMagnitude; // adjusted to game speed

    public ArkanoidGame ArkanoidGame { get; set; }
    GameObject LeftMotorFire, RightMotorFire;
    ParticleSystem[] motorsPS;

    static MiniGameInputVectorMapping[] inputMappings;
    static Type[] collisionTypes = new Type[1] { typeof(ArkanoidFrame) };

    static ArkanoidBase ()
    {
        inputMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.Left, MiniGame.Left),
            new MiniGameInputVectorMapping (MiniGameInput.Right, MiniGame.Right)
        };
    }

    public override void Start()
    {
        base.Start();

        LeftMotorFire = transform.Find("LeftMotorFire").gameObject;
        RightMotorFire = transform.Find("RightMotorFire").gameObject;
        motorsPS = new ParticleSystem[] {
            RightMotorFire.GetComponent<ParticleSystem>(),
            LeftMotorFire.GetComponent<ParticleSystem>()
        };
    }

    public void Config(float gameSpeed)
    {
        adjustedBaseMoveMagnitude = baseMoveMagnitude * gameSpeed;
    }

    public override void Update()
    {
        base.Update();
        if (ArkanoidGame.GamePlaying)
        {
            #region get player input
            for (int i = 0; i < inputMappings.Length; i++)
            {
                MiniGameInputVectorMapping inputMapping = inputMappings[i];
                ParticleSystem motorPS = motorsPS[i];
                bool isPressed = ArkanoidGame.GameController.GetMiniGameInput(ArkanoidGame, inputMapping.MiniGameInput);
                if (isPressed)
                {
                    bool move = TryMove(inputMapping.Vector, adjustedBaseMoveMagnitude * Time.deltaTime, collisionTypes);
                }
                bool isEmitting = motorPS.isEmitting;
                if (isPressed && !isEmitting) {
                    motorPS.Play();
                }
                if (!isPressed && isEmitting) {
                    motorPS.Stop();
                }
            }
            #endregion
        }
    }
}

