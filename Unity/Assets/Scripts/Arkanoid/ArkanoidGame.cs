﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidGame : MiniGame
{
    public GameObject[] ArkanoidBlockPrefabs ;
    public float BlockWidth = 2;
    public float BlockHeight = 1;
    public ArkanoidBase ArkanoidBase;
    public float BaseWidth = 3;
    public float BaseHeight = 1;
    public ArkanoidBall ArkanoidBall;
    public float BallRadius = 0.5f;
    public ArkanoidGameOverZone ArkanoidGameOverZone;

    List<ArkanoidBlock> blocks = new List<ArkanoidBlock>();


    public override void Awake()
    {
        base.Awake();
        LayerMask = 1 << 9;
        GameName = "Arkanoid";

        #region Set objects' parent game
        ArkanoidBall.ArkanoidGame = this;
        ArkanoidBase.ArkanoidGame = this;
        ArkanoidGameOverZone.ArkanoidGame = this;
        #endregion

        #region create blocks
        for (int iter = 0; iter < ArkanoidBlockPrefabs.Length; iter++)
        {
            int row = (int)(rows - ArkanoidBlockPrefabs.Length - iter * BlockHeight);
            for (int column = 0; column < columns; column += (int)BlockWidth)
            {
                GameObject blockGO = InstantiateBlock( ArkanoidBlockPrefabs[iter], column , row );
                ArkanoidBlock block = blockGO.GetComponent<ArkanoidBlock>();
                block.ArkanoidGame = this;
                blocks.Add(block);
            }
        }
        #endregion
    }

    public override void Start()
    {
        base.Start();
        
    }

    public override void Config(GameController GameController, float gameSpeed)
    {
        base.Config(GameController, gameSpeed);
        ArkanoidBall.Config(gameSpeed);
        ArkanoidBase.Config(gameSpeed);
        for (int i = 0; i < blocks.Count; i++) {
            blocks[i].Config(gameSpeed);
        }
    }

    public override void StartGame()
    {
        base.StartGame();
        ArkanoidBall.StartGame();
    }

    GameObject InstantiateBlock( GameObject prefab , int column, int row )
    {
        Vector3 localPosition = CellOriginPosition(column, row) + new Vector3(CellSize / 2f, 0, 0);
        GameObject go = InstantiateObject(prefab, localPosition);
        go.transform.parent = Objects.transform;
        return go;
    }

    public void OnBlockDestroy (ArkanoidBlock block)
    {
        OnScorePoints(block.Points);
    }
}
