﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidBlock : MonoBehaviour
{
    const float MIN_ALPHA_START = 0.6f;
    const float MIN_ALPHA_END = 0.0f;
    const float MAX_ALPHA_START = 0.6f;
    const float MAX_ALPHA_HALF = 1.0f;
    const float MAX_ALPHA_END = 0.0f;

    public int Points = 10;

    public ArkanoidGame ArkanoidGame { get; set; }

    static int minAlphaPropertyId = Shader.PropertyToID("MinAlpha_477F727");
    static int maxAlphaPropertyId = Shader.PropertyToID("MaxAlpha_834D1DBC");

    float destroyAnimationDuration = 2; // default
    float adjustedDestroyAnimationDuration; // adjusted to game speed
    float halfAdjustedDestroyAnimationDuration;

    bool destroying = false;
    float destroyingTime = 0f;

    Material material ;
    BoxCollider boxCollider;

    void Awake() {
        material = GetComponentInChildren<MeshRenderer>().material;
        boxCollider  = GetComponentInChildren<BoxCollider>();
    }

    public void Config(float gameSpeed)
    {
        adjustedDestroyAnimationDuration = destroyAnimationDuration / gameSpeed;
        halfAdjustedDestroyAnimationDuration = adjustedDestroyAnimationDuration / 2f;
    }

    void OnCollisionEnter(Collision collision)
    {
        #region handle collision with ArkanoidBall
        ArkanoidBall ball = collision.gameObject.GetComponent<ArkanoidBall>();
        if (ball != null)
        {
            ArkanoidGame.OnBlockDestroy(this);
            //Destroy(gameObject);
            destroying = true;
            boxCollider.enabled = false;
        }
        #endregion
    }

    void Update()
    {
        if (destroying) {
            destroyingTime += Time.deltaTime;
            if (destroyingTime < halfAdjustedDestroyAnimationDuration) {
                float minAlpha = Mathf.Lerp(MIN_ALPHA_START, MIN_ALPHA_END, 
                    destroyingTime / halfAdjustedDestroyAnimationDuration);
                material.SetFloat(minAlphaPropertyId, minAlpha);
                float maxAlpha = Mathf.Lerp(MIN_ALPHA_START, MAX_ALPHA_HALF, 
                    destroyingTime / halfAdjustedDestroyAnimationDuration);
                material.SetFloat(maxAlphaPropertyId, maxAlpha);
            } else if (destroyingTime < adjustedDestroyAnimationDuration) {
                material.SetFloat(minAlphaPropertyId, MIN_ALPHA_END);
                float maxAlpha = Mathf.Lerp(MAX_ALPHA_HALF, MAX_ALPHA_END, 
                    (destroyingTime - halfAdjustedDestroyAnimationDuration) / halfAdjustedDestroyAnimationDuration);
                material.SetFloat(maxAlphaPropertyId, maxAlpha);
            } else {
                Destroy(gameObject);
            }
        }        
    }
}
