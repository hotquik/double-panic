﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArkanoidBall : MonoBehaviour
{
    Rigidbody m_RigidBody;
    public ArkanoidGame ArkanoidGame { get; set; }

    float ballMoveMagnitude = 4;
    float adjustedBallMoveMagnitude; // adjusted to game speed

    float minXYVelProportion = 2f;
    float xyVelProportionCorrection = 0.05f;

    Vector3 direction;

    void Awake ()
    {
        m_RigidBody = GetComponent<Rigidbody>();
    }

    void Start()
    {
    }

    public void Config(float gameSpeed)
    {
        adjustedBallMoveMagnitude = ballMoveMagnitude * gameSpeed;
    }

    public void StartGame()
    {
        #region Initial impulse on ball
        Vector3 UpLeft = MiniGame.Up + MiniGame.Left;
        Vector3 UpRight = MiniGame.Up + MiniGame.Right;
        float random = Random.Range(0, 100);
        direction = (UpLeft * random + UpRight * (100 - random)).normalized;
        if (m_RigidBody.isKinematic) m_RigidBody.isKinematic = false;
        m_RigidBody.AddForce(direction.normalized * adjustedBallMoveMagnitude, ForceMode.Impulse);
        #endregion
    }

    void Update()
    {
        if (ArkanoidGame.GamePlaying)
        {
            if (m_RigidBody.isKinematic) m_RigidBody.isKinematic = false;
            FixPhysics();
        }
        else
        {
            if (!m_RigidBody.isKinematic) m_RigidBody.isKinematic = true;
        }
    }

    void FixPhysics ()
    {
        #region fix direction
        if (System.Math.Abs(m_RigidBody.velocity.y) * minXYVelProportion < System.Math.Abs(m_RigidBody.velocity.x))
        {
            Vector3 vector = new Vector3(0, m_RigidBody.velocity.y, 0).normalized;
            m_RigidBody.AddForce(vector * adjustedBallMoveMagnitude * xyVelProportionCorrection, ForceMode.Impulse);
        }
        #endregion

        #region preserve ball speed!
        Vector3 velocity = m_RigidBody.velocity;
        float diff = adjustedBallMoveMagnitude - velocity.magnitude;
        velocity = velocity.normalized;
        m_RigidBody.AddForce(velocity * diff, ForceMode.Impulse);
        #endregion
    }
}
