﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class KinematicObject : MonoBehaviour
{
    protected Rigidbody m_RigidBody;
    
    public virtual void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        if (m_RigidBody == null)
        {
            Util.Log("Rigidbody not found", gameObject);
        }
    }

    public virtual void Start()
    {
    }

    public virtual void Update()
    {
    }

    public virtual bool TryMove(Vector3 direction, float magnitude, Type[] collisionTypes = null, Type[] collisionIgnoreTypes = null)
    {
        #region Check if the object can be moved without colliding
        bool collides = SweepTest(direction, magnitude, QueryTriggerInteraction.Collide, collisionTypes, collisionIgnoreTypes);

        if (!collides)
        {
            Move(direction, magnitude);
            return true;
        }
        else
        {
            return false;
        }
        #endregion
    }

    public virtual bool SweepTest(Vector3 direction, float maxDistance = Mathf.Infinity, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal, 
        Type[] collisionTypes = null, Type[] collisionIgnoreTypes = null)
    {
        RaycastHit[] hits = m_RigidBody.SweepTestAll(direction, maxDistance, queryTriggerInteraction);
        
        for (int j = 0; j < hits.Length; j++)
        {
            GameObject otherGameObject = hits[j].transform.gameObject;
            #region if collisionTypes is defined, only collide against these types
            if (collisionTypes != null)
            {
                for (int k = 0; k < collisionTypes.Length; k++)
                {
                    System.Object o = otherGameObject.GetComponent(collisionTypes[k]);
                    if (o != null)
                    {
                        return true; // object does collide for move test !
                    }
                }
            }
            #endregion
            #region else, collide agains anything except collisionIgnoreTypes
            else
            {
                bool ignoreHit = false;
                if (collisionIgnoreTypes != null)
                {
                    for (int k = 0; k < collisionIgnoreTypes.Length && !ignoreHit; k++)
                    {
                        System.Object o = otherGameObject.GetComponent(collisionIgnoreTypes[k]);
                        if (o != null)
                        {
                            ignoreHit = true;
                        }
                    }
                }
                if (!ignoreHit)
                {
                    return true; // object does collide for move test !
                }
            }
            #endregion
        }
        return false;
    }

    public virtual void Move(Vector3 direction, float magnitude)
    {
        transform.position = transform.position + direction * magnitude;
    }
}
