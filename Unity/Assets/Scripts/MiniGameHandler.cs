﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameHandler
{
    public bool active = true;
    public int index ;
    public ScoreBoard scoreBoard;
    public MiniGameMessage miniGameMessage;
    public Vector3 localPosition;
    public KeyMapping[] keyMappings;

    public int totalScore = 0;
    public int prefabIndex = -1;
    public MiniGame prefab;
    public MiniGame miniGame;

    public MiniGameHandler(int index, ScoreBoard scoreBoard, MiniGameMessage miniGameMessage , Vector3 localPosition, KeyMapping[] keyMappings)
    {
        this.index = index;
        this.scoreBoard = scoreBoard;
        this.miniGameMessage = miniGameMessage;
        this.localPosition = localPosition;
        this.keyMappings = keyMappings;
    }

    public float MessageAnimationTime { get => MiniGameMessage.DefaultAnimationTime / miniGame.GameSpeed;  }
}