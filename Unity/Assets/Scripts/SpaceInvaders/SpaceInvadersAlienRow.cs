﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpaceInvadersAlienRow : KinematicObject
{
    public MiniGameInput Direction;
    public bool CanFire;
    public SpaceInvadersAlien AlienPrefab;

    float rowMoveMagnitude = 1;
    float adjustedRowMoveMagnitude; // adjusted to game speed

    private bool aliensCreated = false;
    private SpaceInvadersGame spaceInvadersGame;
    public SpaceInvadersGame SpaceInvadersGame { 
        get {
            return spaceInvadersGame;
        }   
        set
        {
            spaceInvadersGame = value;

            InstantiateAliens();
        }
    }

    Type[] collisionTypes = new Type[1] { typeof(SpaceInvadersFrame) };

    public override void Awake ()
    {
        base.Awake();
        InstantiateAliens();
    }

    private void InstantiateAliens ()
    {
        if (!aliensCreated && SpaceInvadersGame != null)
        {

            #region Initialize aliens in row
            List<Transform> transforms = Util.FindTransformsByTag(transform, "Marker");
            for (int i = 0; i < transforms.Count; i++)
            {
                GameObject markerGo = transforms[i].gameObject;
                markerGo.GetComponent<MeshRenderer>().enabled = false;
                GameObject alienGo = SpaceInvadersGame.InstantiateObject(AlienPrefab.gameObject, Vector3.zero, Quaternion.identity, markerGo);
                SpaceInvadersAlien alien = alienGo.GetComponent<SpaceInvadersAlien>();
                alien.SpaceInvadersGame = SpaceInvadersGame;
                alien.CanFire = CanFire;
                alien.SpaceInvadersAlienRow = this;
            }
            #endregion

            aliensCreated = true;
        }
    }

    public override void Start()
    {
        base.Start();
    }

    public void Config(float gameSpeed)
    {
        adjustedRowMoveMagnitude = rowMoveMagnitude * gameSpeed;
    }

    public override void Update()
    {
        base.Update();
        if (SpaceInvadersGame.GamePlaying)
        {
            #region Move the row
            bool move = true;

            float magnitude = adjustedRowMoveMagnitude * Time.deltaTime;

            if (Direction == MiniGameInput.Left)
            {
                move = TryMove(MiniGame.Left, magnitude, collisionTypes);
            }
            if (Direction == MiniGameInput.Right)
            {
                move = TryMove(MiniGame.Right, magnitude, collisionTypes);
            }
            if ( !move )
            {
                ChangeDirection();
            }
            #endregion
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        SpaceInvadersFrame frame = other.gameObject.GetComponent<SpaceInvadersFrame>();
        if (frame != null)
        {
            ChangeDirection();
        }
    }

    void ChangeDirection ()
    {
        float magnitude = adjustedRowMoveMagnitude * Time.deltaTime;
        bool move = TryMove(MiniGame.Down, magnitude, collisionTypes);
        Direction = (Direction == MiniGameInput.Left ? MiniGameInput.Right : MiniGameInput.Left);
    }
}
