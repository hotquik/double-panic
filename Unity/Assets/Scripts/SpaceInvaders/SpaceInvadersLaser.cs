﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersLaser : MonoBehaviour
{
    float laserMoveMagnitude = 4;
    float adjustedLaserMoveMagnitude; // adjusted to game speed

    Vector3 direction;
    bool fired = false;

    public bool ShipFire { get; set; }
    public bool AlienFire { get; set; }
    public SpaceInvadersGame SpaceInvadersGame { get; set; }
    
    void Start()
    {
    }

    public void Config(float gameSpeed)
    {
        adjustedLaserMoveMagnitude = laserMoveMagnitude * gameSpeed;
    }

    void Update()
    {
        if (SpaceInvadersGame.GamePlaying)
        {
            if (fired)
            {
                transform.position += direction * adjustedLaserMoveMagnitude * Time.deltaTime;
            }
        }
    }

    public void Fire (Vector3 direction)
    {
        this.direction = direction;
        fired = true;
    }

    void OnTriggerEnter(Collider other)
    {
        #region Ignore Collision with AlienRow hits
        SpaceInvadersAlienRow alienRow = other.gameObject.GetComponent<SpaceInvadersAlienRow>();
        if (alienRow != null)
        {
           return;
        }
        #endregion

        #region Ignore Friendly Fire
        if (AlienFire)
        {
            SpaceInvadersAlien alien = other.gameObject.GetComponent<SpaceInvadersAlien>();
            if (alien != null)
            {
                return;
            }
        }
        #endregion
        Destroy(gameObject);
    }
}
