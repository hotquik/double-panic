﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersAlien : MonoBehaviour
{
    public int Points = 10;
    public SpaceInvadersLaser SpaceInvadersLaserPrefab;

    public float fireProbability = 0.05f;

    public SpaceInvadersGame SpaceInvadersGame { get; set; }
    public SpaceInvadersAlienRow SpaceInvadersAlienRow { get; set; }
    public SpaceInvadersAlienDestroyedEffect DestroyedEffectPrefab;
    public bool CanFire { get; set; }

    GameObject m_laserMarker;
    bool destroyed = false;

    void Start()
    {
        m_laserMarker = gameObject.transform.Find("LaserMarker").gameObject;
    }

    void Update()
    {
        if (SpaceInvadersGame.GamePlaying)
        {
            if (CanFire && Random.value * 100f < fireProbability * SpaceInvadersGame.GameSpeed)
            {
                FireLaser();
            }
        }
    }

    SpaceInvadersLaser FireLaser()
    {
        GameObject go = SpaceInvadersGame.InstantiateObject(SpaceInvadersLaserPrefab.gameObject, m_laserMarker.transform.position, false);
        SpaceInvadersLaser laser = go.GetComponent<SpaceInvadersLaser>();
        laser.SpaceInvadersGame = SpaceInvadersGame;
        laser.Config(SpaceInvadersGame.GameSpeed);
        laser.AlienFire = true;
        laser.Fire(MiniGame.Down);
        return laser;
    }
    
    void OnTriggerEnter(Collider other)
    {
        SpaceInvadersLaser laser = other.gameObject.GetComponent<SpaceInvadersLaser>();
        if (!destroyed && laser != null && laser.ShipFire)
        {
            destroyed = true;
            SpaceInvadersGame.OnScorePoints(Points);
            Destroy(gameObject);
            // create 'on destroyed' anim
            SpaceInvadersGame.InstantiateObject(DestroyedEffectPrefab.gameObject, transform.position, false);
        }
    }
}
