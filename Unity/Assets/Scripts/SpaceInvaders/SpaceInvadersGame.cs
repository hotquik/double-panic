﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceInvadersGame : MiniGame
{
    public SpaceInvadersShip SpaceInvadersShip;
    SpaceInvadersAlienRow[] AlienRows;
    
    public override void Awake()
    {
        base.Awake();
        LayerMask = 1 << 11;
        GameName = "Space Invaders";

        #region Set objects' parent game
        AlienRows = GetComponentsInChildren<SpaceInvadersAlienRow>();
        SpaceInvadersShip.SpaceInvadersGame = this;
        for( int i = 0; i < AlienRows.Length; i ++)
        {
            AlienRows[i].SpaceInvadersGame = this;
        }
        #endregion
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Config(GameController GameController, float gameSpeed)
    {
        base.Config(GameController, gameSpeed);
        SpaceInvadersShip.Config(gameSpeed);
        if (AlienRows != null)
        {
            for (int i = 0; i < AlienRows.Length; i++)
            {
                AlienRows[i].Config(gameSpeed);
            }
        }
    }

}
