﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpaceInvadersShip : KinematicObject
{
    public SpaceInvadersLaser SpaceInvadersLaserPrefab;

    public float shipMoveMagnitude = 4;
    float adjustedShipMoveMagnitude; // adjusted to game speed
    GameObject m_laserMarker;

    public float MaxRotationAngle = 30;
    public float shipRotationMagnitude = 5;
    float adjustedShipRotationMagnitude; // adjusted to game speed

    public SpaceInvadersGame SpaceInvadersGame { get; set; }

    Type[] collisionTypes = new Type[1] { typeof(SpaceInvadersFrame) };

    public override void Start()
    {
        base.Start();
        m_laserMarker = gameObject.transform.Find("LaserMarker").gameObject;
    }
    
    public void Config(float gameSpeed)
    {
        adjustedShipMoveMagnitude = shipMoveMagnitude * gameSpeed;
        adjustedShipRotationMagnitude = shipRotationMagnitude * gameSpeed;
    }
    
    public override void Update()
    {
        base.Update();
        if (SpaceInvadersGame.GamePlaying)
        {
            #region get player input
            bool move, rotate, input = false;
            //bool fire;
            float magnitude = adjustedShipMoveMagnitude * Time.deltaTime;
            float rotMagnitude = adjustedShipRotationMagnitude * Time.deltaTime;
            if (SpaceInvadersGame.GameController.GetMiniGameInput(SpaceInvadersGame, MiniGameInput.Left))
            {
                move = TryMove(MiniGame.Left, magnitude, collisionTypes);
                rotate = TryRotate(MiniGame.Up, rotMagnitude);
                input = true;
            }
            if (SpaceInvadersGame.GameController.GetMiniGameInput(SpaceInvadersGame, MiniGameInput.Right))
            {
                move = TryMove(MiniGame.Right, magnitude, collisionTypes);
                rotate = TryRotate(MiniGame.Down, rotMagnitude);
                input = true;
            }
            if (!input) {
                ResetRotation(rotMagnitude);
            }
            if (SpaceInvadersGame.GameController.GetMiniGameInputDown(SpaceInvadersGame, MiniGameInput.Up))
            {
                /*fire =*/ FireLaser();
            }

            #endregion
        }
    }

    public void ResetRotation (float magnitude) {
        /*
        Vector3 direction;
        bool positive1 , positive2;
        if (transform.localEulerAngles.y > 180) {
            direction = MiniGame.Up;
            positive1 = false;
        } else {
            direction = MiniGame.Down;
            positive1 = true;
        }
        Vector3 newlocalEulerAngles = transform.localEulerAngles + direction * magnitude;
        float newY = newlocalEulerAngles.y % 360;
        if (newY > 180) {
            positive2 = false;
        } else {
            positive2 = true;
        }
        if (positive1 != positive2) {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 0, transform.localEulerAngles.z); 
        }
        else {
            TryRotate(direction, magnitude);
        }
        */

        Vector3 direction;
        float y = (transform.localEulerAngles.y > 180 ? transform.localEulerAngles.y - 360 : transform.localEulerAngles.y);
        if (y == 0) {
            // do nothing
        } else if (Math.Abs(y) < magnitude) {
            // set y to 0
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 0, transform.localEulerAngles.z); 
        } else {
            if (y < 0) {
                direction = MiniGame.Up;
            } else {
                direction = MiniGame.Down;
            }
            TryRotate(direction, magnitude);
        }
    }

    public bool TryRotate(Vector3 direction, float magnitude)
    {
        Vector3 newlocalEulerAngles = transform.localEulerAngles + direction * magnitude;
        float newY = newlocalEulerAngles.y % 360;
        if (newY > MaxRotationAngle && newY < 360 - MaxRotationAngle)
        {
            return false;
        }
        else
        {
            Rotate(direction, magnitude);
            return true;
        }
    }

    void Rotate(Vector3 direction, float magnitude)
    {
        transform.localEulerAngles = transform.localEulerAngles + direction * magnitude;
    }
    
    SpaceInvadersLaser FireLaser()
    {
        GameObject go = SpaceInvadersGame.InstantiateObject(SpaceInvadersLaserPrefab.gameObject, m_laserMarker.transform.position, false);
        SpaceInvadersLaser laser = go.GetComponent<SpaceInvadersLaser>();
        laser.SpaceInvadersGame = SpaceInvadersGame;
        laser.Config(SpaceInvadersGame.GameSpeed);
        laser.ShipFire = true;
        laser.Fire(MiniGame.Up);
        return laser;
    }
    
    void OnTriggerEnter(Collider other)
    {
        #region Collision with alien laser
        SpaceInvadersLaser laser = other.gameObject.GetComponent<SpaceInvadersLaser>();
        if (laser != null && laser.AlienFire)
        {
            SpaceInvadersGame.OnGameOver();
        }
        #endregion Collision with alien laser

        #region Collision with alien 
        SpaceInvadersAlien alien = other.gameObject.GetComponent<SpaceInvadersAlien>();
        if (alien != null)
        {
            SpaceInvadersGame.OnGameOver();
        }
        #endregion Collision with alien 
    }
}
