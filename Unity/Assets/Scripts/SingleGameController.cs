﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SingleGameController : GameController
{
    public MiniGame miniGame;
    KeyMapping[] keyMappings;

    public override void Start()
    {
        keyMappings = new KeyMapping[]
        {
            new KeyMapping (KeyCode.W, MiniGameInput.Up),
            new KeyMapping (KeyCode.A, MiniGameInput.Left),
            new KeyMapping (KeyCode.S, MiniGameInput.Down),
            new KeyMapping (KeyCode.D, MiniGameInput.Right),

            new KeyMapping (KeyCode.UpArrow, MiniGameInput.Up),
            new KeyMapping (KeyCode.LeftArrow, MiniGameInput.Left),
            new KeyMapping (KeyCode.DownArrow, MiniGameInput.Down),
            new KeyMapping (KeyCode.RightArrow, MiniGameInput.Right),
        };
        miniGame.Config( this, GameSpeed);
        miniGame.StartGame();
    }

    public override bool GetMiniGameInputDown(MiniGame minigame, MiniGameInput miniGameInput)
    {
        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKeyDown(keyMappings[i].KeyCode);
            }
        }
        return result;
    }

    public override bool GetMiniGameInputUp(MiniGame minigame, MiniGameInput miniGameInput)
    {
        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKeyUp(keyMappings[i].KeyCode);
            }
        }
        return result;
    }

    public override bool GetMiniGameInput(MiniGame minigame, MiniGameInput miniGameInput)
    {
        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKey(keyMappings[i].KeyCode);
            }
        }
        return result;
    }
}