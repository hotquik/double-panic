﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeGame : MiniGame
{
    public SnakeBody SnakeBody;
    public SnakeApple SnakeApplePrefab;

    public override void Awake()
    {
        base.Awake();
        GameName = "Snake";
        LayerMask = 1 << 10;

        SnakeBody.SnakeGame = this;
    }

    public override void Start()
    {
        base.Start();
        CreateApple();
    }

    public override void Config(GameController GameController, float gameSpeed)
    {
        base.Config(GameController, gameSpeed);
        SnakeBody.Config(gameSpeed);
    }
    
    public void OnAppleHit (SnakeApple apple)
    {
        OnScorePoints(apple.Points);
        Destroy(apple.gameObject);
        // generate new apple
        CreateApple();
    }

    void CreateApple()
    {
        bool created = false;

        while (!created)
        {
            int row = Random.Range(0, Rows);
            int column = Random.Range(0, Columns);

            RaycastHit[] hits = SweepTestCell(column, row, QueryTriggerInteraction.Collide);
            if (hits.Length == 0)
            {
                GameObject apple = InstantiateObject(SnakeApplePrefab.gameObject, CellOriginPosition(column , row));
                created = true;
            }
        }
    }
}
