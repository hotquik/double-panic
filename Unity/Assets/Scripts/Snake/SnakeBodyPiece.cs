﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBodyPiece : MonoBehaviour
{
    public Rigidbody rigidBody;
    public MiniGameInput Direction;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }
}
