﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBody : MonoBehaviour
{
    public SnakeBodyPiece SnakeBodyHead;
    public SnakeBodyPiece SnakeBodyCylinderPrefab;
    public SnakeBodyPiece SnakeBodyArcPrefab;
    public SnakeBodyPiece SnakeBodyTail;

    Queue<SnakeBodyPiece> innerBody = new Queue<SnakeBodyPiece>();

    MiniGameInput currentDirection = MiniGameInput.Right;
    Vector3 currentDirectionVector = MiniGame.Right;

    float timeUntilSnakeMove;
    float snakeMoveMagnitude = 1;
    float snakeRotateMagnitude = 90;
    float snakeMoveTimeInterval = 1; // 1 sec default
    float adjustedSnakeMoveTimeInterval; // adjusted to game speed
    MiniGameInputVectorMapping[] inputMappings;
    bool signalGrow = false;

    public SnakeGame SnakeGame { get; set; }

    void Start()
    {
        #region Create input mappings
        inputMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.Up, MiniGame.Up),
            new MiniGameInputVectorMapping (MiniGameInput.Left, MiniGame.Left),
            new MiniGameInputVectorMapping (MiniGameInput.Right, MiniGame.Right),
            new MiniGameInputVectorMapping (MiniGameInput.Down, MiniGame.Down)
        };
        #endregion
    } 

    public void Config(float gameSpeed)
    {
        adjustedSnakeMoveTimeInterval = snakeMoveTimeInterval / gameSpeed;
        timeUntilSnakeMove = adjustedSnakeMoveTimeInterval;        
    }

    private bool AllowedInput(MiniGameInput input)
    {
        if ((input == MiniGameInput.Up || input == MiniGameInput.Down) && (SnakeBodyHead.Direction == MiniGameInput.Up || SnakeBodyHead.Direction == MiniGameInput.Down))
        {
            return false;
        }
        if ((input == MiniGameInput.Left || input == MiniGameInput.Right) && (SnakeBodyHead.Direction == MiniGameInput.Left || SnakeBodyHead.Direction == MiniGameInput.Right))
        {
            return false;
        }
        return true;
    }

    void Update()
    {
        if (SnakeGame.GamePlaying)
        {
            #region get player input
            Vector3 pendingVector = Vector3.zero;
            for (int i = 0; i < inputMappings.Length; i++)
            {
                MiniGameInput pendingInput = inputMappings[i].MiniGameInput;
                if (AllowedInput(pendingInput) && SnakeGame.GameController.GetMiniGameInputDown(SnakeGame, pendingInput))
                {
                    currentDirection = pendingInput;
                    currentDirectionVector = inputMappings[i].Vector;
                    break;
                }
            }
            #endregion

            #region Process player input
            timeUntilSnakeMove -= Time.deltaTime;
            if (timeUntilSnakeMove < 0)
            {
                timeUntilSnakeMove += adjustedSnakeMoveTimeInterval;
                bool move = TryMove(currentDirection, currentDirectionVector, snakeMoveMagnitude );
                if (!move)
                {
                    SnakeGame.OnGameOver();
                }
            }
            #endregion
        }
    }
   
    bool TryMove(MiniGameInput input, Vector3 direction, float magnitude)
    {
        #region Check if the move can be done
        bool signalAteApple = false;
        RaycastHit[] hits = SnakeBodyHead.rigidBody.SweepTestAll(direction, magnitude, QueryTriggerInteraction.Collide);
        for (int j = 0; j < hits.Length; j++)
        {
            RaycastHit hit = hits[0];            
            GameObject otherGameObject = hits[j].transform.gameObject;
            #region hits against an apple
            SnakeApple apple = otherGameObject.GetComponent<SnakeApple>();
            if (apple != null)
            {
                signalAteApple = true;
                SnakeGame.OnAppleHit(apple);
                continue;
            }
            #endregion
            return false;
        }
        #endregion

        #region Create new piece at first position
        if (innerBody.Count > 0 || signalGrow)
        {
            SnakeBodyPiece firstBodyPiece;
            #region No direction change, insert a cylinder
            if (input == SnakeBodyHead.Direction)
            {
                firstBodyPiece = InstantiateBodyPiece(SnakeBodyCylinderPrefab, SnakeBodyHead.transform.position);
                firstBodyPiece.Direction = SnakeBodyHead.Direction;
                RotateBodyPiece(firstBodyPiece, firstBodyPiece.Direction);
            }
            #endregion
            #region Direction change, insert an arc
            else
            {
                firstBodyPiece = InstantiateBodyPiece(SnakeBodyArcPrefab, SnakeBodyHead.transform.position);
                firstBodyPiece.Direction = SnakeBodyHead.Direction;
                #region Clockwise rotation
                if ( ((int)SnakeBodyHead.Direction + 1) % 4 == (int) input )
                {
                    RotateBodyPiece(firstBodyPiece, firstBodyPiece.Direction);
                }
                #endregion
                #region Counterclockwise rotation
                else
                {
                    RotateBodyPiece(firstBodyPiece, firstBodyPiece.Direction + 1);
                }
                #endregion
            }
            #endregion
            innerBody.Enqueue(firstBodyPiece);
        }
        #endregion

        #region Relocate and rotate tail
        if (!signalGrow)
        {
            #region Destroy last piece
            SnakeBodyPiece lastBodyPiece;
            if (innerBody.Count > 0)
            {
                lastBodyPiece = innerBody.Dequeue();
                Destroy(lastBodyPiece.gameObject);
                SnakeBodyTail.transform.position = lastBodyPiece.transform.position;
            }
            else
            {
                SnakeBodyTail.transform.position = SnakeBodyHead.transform.position;
            }
            #endregion

            #region If pieces exist, use last piece as template . otherwise, use the head
            SnakeBodyTail.Direction = (innerBody.Count > 0 ?  innerBody.Peek().Direction : input);
            RotateBodyPiece(SnakeBodyTail, SnakeBodyTail.Direction);
            #endregion
        }
        #endregion

        #region Relocate and rotate head
        SnakeBodyHead.transform.position = SnakeBodyHead.transform.position + direction * magnitude;
        SnakeBodyHead.Direction = input;
        RotateBodyPiece(SnakeBodyHead, SnakeBodyHead.Direction);
        #endregion 

        // Signal grow for next update
        signalGrow = signalAteApple;
        return true;
    }       

    void RotateBodyPiece(SnakeBodyPiece bodyPiece, MiniGameInput input)
    {
        int turns = 0;
        switch(input)
        {
            case MiniGameInput.Right:
                turns = 0;
                break;
            case MiniGameInput.Down:
                turns = 1;
                break;
            case MiniGameInput.Left:
                turns = 2;
                break;
            case MiniGameInput.Up:
                turns = 3;
                break;
        }
        bodyPiece.transform.localEulerAngles = MiniGame.Clockwise * snakeRotateMagnitude * turns;
    }

    SnakeBodyPiece InstantiateBodyPiece(SnakeBodyPiece prefab, Vector3 position)
    {
        GameObject go = Instantiate(prefab.gameObject, Vector3.zero, Quaternion.identity);
        go.transform.parent = transform;
        go.transform.position = position;
        return go.GetComponent< SnakeBodyPiece>();
    }
}
