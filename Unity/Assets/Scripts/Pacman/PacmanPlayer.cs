﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PacmanPlayer : PacmanActor
{
    public bool OnSpecialPillEffect = false;
    public float SpecialPillDuration = 20F;
    
    float AdjustedSpecialPillDuration;
    float SpecialPillTimeLeft = 0F;

    bool goingLeft = false;
    Vector3 leftRotation = new Vector3(0, 180, 0);

    public override void Awake()
    {
        base.Awake();

        #region Create input rotation mappings
        inputRotationMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.None, Vector3.zero),
            new MiniGameInputVectorMapping (MiniGameInput.Left, leftRotation),
            new MiniGameInputVectorMapping (MiniGameInput.Right, Vector3.zero),
            new MiniGameInputVectorMapping (MiniGameInput.Up, new Vector3(0,0,90)),
            new MiniGameInputVectorMapping (MiniGameInput.Down, new Vector3(0,0,-90))
        };
        #endregion
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Config(float gameSpeed)
    {
        base.Config(gameSpeed);
        AdjustedSpecialPillDuration = SpecialPillDuration / gameSpeed;
    }

    public override void Update()
    {
        MiniGameInputVectorMapping lastInput = currentInput;
        base.Update();

        if (PacmanGame.GamePlaying)
        {
            #region update mesh rotation
            if (currentInput != null && lastInput != currentInput)
            {
                //MiniGameInputVectorMapping lastRotationInput = Array.Find(inputRotationMappings, item => item.MiniGameInput == lastInput.MiniGameInput);
                MiniGameInputVectorMapping currentRotationInput = Array.Find(inputRotationMappings, item => item.MiniGameInput == currentInput.MiniGameInput);
                Vector3 rotation = currentRotationInput.Vector;
                switch (currentRotationInput.MiniGameInput)
                {
                    case MiniGameInput.Left:
                        goingLeft = true;
                        break;
                    case MiniGameInput.Right:
                        goingLeft = false;
                        break;
                    case MiniGameInput.Up:
                        rotation += (goingLeft ? leftRotation : Vector3.zero);
                        break;
                    case MiniGameInput.Down:
                        rotation += (goingLeft ? leftRotation : Vector3.zero);
                        break;
                }
                transform.localEulerAngles = rotation;
            }
            #endregion

            #region check special effects
            if (OnSpecialPillEffect)
            {
                SpecialPillTimeLeft -= Time.deltaTime;
                if (SpecialPillTimeLeft <= 0)
                {
                    OnSpecialPillEffectEnd();
                }
                else if  (SpecialPillTimeLeft <= AdjustedSpecialPillDuration * 0.20)
                {
                    PacmanGame.OnSpecialPillEffectEnding();
                }
            }
            #endregion

            #region get player input
            for (int i = 0; i < inputDirectionMappings.Length; i++)
            {
                if (PacmanGame.GameController.GetMiniGameInput(PacmanGame, inputDirectionMappings[i].MiniGameInput))
                {
                    PendingInput = inputDirectionMappings[i];
                }
            }
            #endregion
        }

    }

    /*
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("PacmanPlayer.OnCollisionEnter()");
       
    }
    */

    public override void OnTriggerEnter(Collider other)
    {
        Log("PacmanPlayer<" + name + ">.OnTriggerEnter(" + other.gameObject.name + ")");
        base.OnTriggerEnter(other);
        #region collision with PacmanNormalPill
        PacmanNormalPill pacmanNormalPill = other.gameObject.GetComponent<PacmanNormalPill>();
        if (pacmanNormalPill != null)
        {
            PacmanGame.OnScorePoints(pacmanNormalPill.Points);
            Destroy(pacmanNormalPill.gameObject);
        }
        #endregion

        #region collision with PacmanSpecialPill
        PacmanSpecialPill pacmanSpecialPill = other.gameObject.GetComponent<PacmanSpecialPill>();
        if (pacmanSpecialPill != null)
        {
            OnSpecialPillEffectStart(pacmanSpecialPill);
            PacmanGame.OnScorePoints(pacmanSpecialPill.Points);
            Destroy(pacmanSpecialPill.gameObject);
        }
        #endregion

        #region collision with PacmanGhost
        PacmanGhost pacmanGhost = other.gameObject.GetComponent<PacmanGhost>();
        if (pacmanGhost != null)
        {
            if (OnSpecialPillEffect && pacmanGhost.CurrentState == PacmanGhost.State.Frightened)
            {
                PacmanGame.OnScorePoints(pacmanGhost.Points);
                //Destroy(pacmanGhost.gameObject);
                pacmanGhost.Die();
            }
            else
            {
                PacmanGame.OnGameOver();
            }
        }
        #endregion
    }

    public void OnSpecialPillEffectStart(PacmanSpecialPill pacmanSpecialPill)
    {
        Log("PacmanPlayer.OnSpecialPillEffectStart()");
        SpecialPillTimeLeft = AdjustedSpecialPillDuration;
        OnSpecialPillEffect = true;
        PacmanGame.OnSpecialPillEffectStart(pacmanSpecialPill);
    }
    public void OnSpecialPillEffectEnd()
    {
        Log("PacmanPlayer.OnSpecialPillEffectEnd()");
        OnSpecialPillEffect = false;
        PacmanGame.OnSpecialPillEffectEnd();
    }


}
