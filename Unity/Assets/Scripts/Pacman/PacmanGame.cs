﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanGame : MiniGame
{
    public GameObject PacmanNormalPillPrefab;

    public PacmanPlayer PacmanPlayer { get; set; }
    public PacmanGhost[] PacmanGhosts { get; set; }
    public PacmanFrame PacmanFrame { get; set; }
    
    public override void Awake()
    {
        columns = 17;
        base.Awake();
        LayerMask = 1 << 12;
        GameName = "Pacman";

        #region Create normal pills
        for (int column = 0; column < Columns; column++)
        {
            for (int row = 0; row < Rows; row++)
            {
                RaycastHit[] hits = SweepTestCell(column, row, QueryTriggerInteraction.Collide, false);
                if (hits.Length == 0)
                {
                    Vector3 localPosition = CellOriginPosition(column, row);
                    GameObject pill = InstantiateObject(PacmanNormalPillPrefab, localPosition);
                }
            }
        }
        #endregion

        #region Set objects' parent game
        PacmanPlayer = GetComponentInChildren<PacmanPlayer>();
        PacmanGhosts = GetComponentsInChildren<PacmanGhost>();
        PacmanFrame = GetComponentInChildren<PacmanFrame>();

        PacmanFrame.PacmanGame = this;
        PacmanPlayer.PacmanGame = this;

        for (int i = 0; i < PacmanGhosts.Length; i++)
        {
            PacmanGhosts[i].PacmanGame = this;
        }
        #endregion
    }
    
    public override void Config(GameController GameController, float gameSpeed)
    {
        base.Config(GameController, gameSpeed);
        PacmanPlayer.Config(gameSpeed);
        for (int i = 0; i < PacmanGhosts.Length; i++)
        {
            PacmanGhosts[i].Config(gameSpeed);
        }
    }
        
    public void OnSpecialPillEffectStart(PacmanSpecialPill pacmanSpecialPill)
    {
        for (int i = 0; i < PacmanGhosts.Length; i++)
        {
            PacmanGhosts[i].CurrentState = PacmanGhost.State.Frightened;
        }
    }


    public void OnSpecialPillEffectEnding()
    {
        for (int i = 0; i < PacmanGhosts.Length; i++)
        {
            PacmanGhosts[i].SignalFrightenedEnding = true;
        }
    }

    public void OnSpecialPillEffectEnd()
    {
        for (int i = 0; i < PacmanGhosts.Length; i++)
        {
            PacmanGhosts[i].CurrentState = PacmanGhost.State.Chase;
        }
    }
}
