﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PacmanGhost : PacmanActor
{
    public enum State {
        Default,
        Chase,
        Scatter,
        Frightened,
        Respawning
    }
       
    public int Points = 10;
    public Material defaultMaterial;
    public Material alternativeMaterial;
    bool defaultMaterialActive = false;

    MeshRenderer bodyMeshRenderer;

    float inputProb = 5;
    int spawnColumn, spawnRow;
    float lockToGridThreshold;

    bool signalFrightenedEnding = false;
    public bool SignalFrightenedEnding { get => signalFrightenedEnding; 
        set
        {
            if (signalFrightenedEnding == value) return;
            #region signal can only be turned on when the ghost is frightened
            if (value && CurrentState != State.Frightened) { return; }
            #endregion
            signalFrightenedEnding = value;
        }
    }
    int frame, frameBlinkingInterval = 3;
    
    State state = State.Default;
    public State CurrentState { get => state;
        set
        {
            if (state == value) return;
            #region When respawning, it can only return to default state
            if (state == State.Respawning && value != State.Default) return;
            #endregion

            state = value;
            SignalFrightenedEnding = false;
            if (PacmanGame != null)
            {
                Config(PacmanGame.GameSpeed / (state == State.Frightened ?  2 : 1));
            }
            UpdateRender();
        }
    }
    
    MiniGameInputVectorMapping[] inputEyeRotationMappings;

    public GameObject LeftEye, RightEye, Body, FrightenedFace;
    private Vector3 m_LeftEyeBaseRotation, m_RightEyeBaseRotation;

    public override void Awake()
    {
        base.Awake();
        collisionTypes = new Type[2] { typeof(PacmanFrame), typeof(PacmanGhost) };

        bodyMeshRenderer = Body.GetComponentInChildren<MeshRenderer>();
        //materials = (defaultMaterial, alternativeMaterial);
        SetBodyMaterial(true);

        #region set input rotation mappings
        inputEyeRotationMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.None, Vector3.zero),
            new MiniGameInputVectorMapping (MiniGameInput.Left, new Vector3(0,0,180)),
            new MiniGameInputVectorMapping (MiniGameInput.Right, Vector3.zero),
            new MiniGameInputVectorMapping (MiniGameInput.Up, new Vector3(0,0,90)),
            new MiniGameInputVectorMapping (MiniGameInput.Down, new Vector3(0,0,-90))
        };
        m_LeftEyeBaseRotation = LeftEye.transform.localEulerAngles;
        m_RightEyeBaseRotation = RightEye.transform.localEulerAngles;
        #endregion
    }

    private void UpdateRender ()
    {
        switch  (state)
        {
            case State.Chase:
                Body.SetActive(true);
                SetBodyMaterial(true);
                LeftEye.SetActive(true);
                RightEye.SetActive(true);
                FrightenedFace.SetActive(false);
                break;
            case State.Scatter:
                Body.SetActive(true);
                SetBodyMaterial(true);
                LeftEye.SetActive(true);
                RightEye.SetActive(true);
                FrightenedFace.SetActive(false);
                break;
            case State.Frightened:
                Body.SetActive(true);
                SetBodyMaterial(false);
                LeftEye.SetActive(false);
                RightEye.SetActive(false);
                FrightenedFace.SetActive(true);
                break;
            case State.Respawning:
                Body.SetActive(false);
                LeftEye.SetActive(false);
                RightEye.SetActive(false);
                FrightenedFace.SetActive(true);
                break;
        }
    }

    private void SetBodyMaterial (bool setDefaultMaterial)
    {
        defaultMaterialActive = setDefaultMaterial;
        Material material = (defaultMaterialActive ? defaultMaterial : alternativeMaterial);
        Material[] mats = bodyMeshRenderer.materials;
        mats[0] = material;
        bodyMeshRenderer.materials = mats;
    }

    private void SwapBodyMaterial()
    {
        SetBodyMaterial(!defaultMaterialActive);
    }

    public override void Start()
    {
        base.Start();
        spawnColumn = column;
        spawnRow = row;

        lockToGridThreshold = PacmanGame.CellSize * 0.05f;
    }

    // Update is called once per frame
    public override void Update()
    {
        MiniGameInputVectorMapping lastInput = currentInput;
        base.Update();
        if (PacmanGame.GamePlaying)
        {
            if (CurrentState == State.Default)
            {
                CurrentState = State.Chase;
            }
            #region Update mesh rotation
            if (currentInput != null && lastInput != currentInput)
            {
                MiniGameInputVectorMapping currentRotationInput = Array.Find(inputEyeRotationMappings, item => item.MiniGameInput == currentInput.MiniGameInput);
                LeftEye.transform.localEulerAngles = m_LeftEyeBaseRotation + currentRotationInput.Vector;
                RightEye.transform.localEulerAngles = m_RightEyeBaseRotation + currentRotationInput.Vector;
            }
            #endregion

            #region Check ghost input
            if (CurrentState != State.Respawning) {
                for (int i = 0; i < inputDirectionMappings.Length; i++)
                {
                    MiniGameInputVectorMapping inputMapping = inputDirectionMappings[i];

                    if (inputMapping != currentInput
                        && inputMapping.MiniGameInput != MiniGame.OppositeInput(currentInput == null ? MiniGameInput.None : currentInput.MiniGameInput)
                        && UnityEngine.Random.value * 100 < inputProb
                        )
                    {
                        PendingInput = inputMapping;
                        break;
                    }
                }
            }
            #endregion

            #region Check Frightened ending blinking
            frame++;
            if (CurrentState == State.Frightened && SignalFrightenedEnding && (frame % frameBlinkingInterval) == 0)
            {
                SwapBodyMaterial();
            }
            #endregion
        }
    }


    public void Die ()
    {
        #region Create respawning movement
        MiniGame.Cell fromCell = PacmanGame.GetCell(column, row);
        MiniGame.Cell toCell = PacmanGame.GetCell(spawnColumn, spawnRow);

        currentInput = new MiniGameInputVectorMapping(MiniGameInput.None, Vector3.zero);
        currentMovement = new Movement(fromCell, toCell, currentInput.MiniGameInput);
        currentMovement.lockToGrid = false;
        currentInput.Vector = currentMovement.Direction;

        currentCell = null;
        lockedToGrid = false;

        PendingInput = null;

        CurrentState = State.Respawning;

        Log("Start Respawning");
        #endregion
    }

    public override void FinishMovement()
    {
        base.FinishMovement();

        #region Finish respawning movement
        if (CurrentState == State.Respawning && (currentMovement.To.Origin - transform.localPosition).magnitude < lockToGridThreshold) 
        {
            LockToGrid();
            currentInput = null;

            CurrentState = State.Default;
            Log("Finished Respawning");
        }
        #endregion
    }
}
