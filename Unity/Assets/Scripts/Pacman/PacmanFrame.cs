﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PacmanFrame : MonoBehaviour
{
    private PacmanGame m_pacmanGame ;
    public PacmanGame PacmanGame { get {
            return m_pacmanGame;
        } 
        set {
            m_pacmanGame = value;
            ScanWalls();
        }
    }

    /// <summary>
    /// Walls: bool[column][row] matrix that indicates if there is a wall on that cell
    /// Used for moving actors, since Physics arent accurate enough
    /// </summary>
    public static bool[][] Walls = null;

    void ScanWalls ()
    {
        #region create Walls based on frame collider
        if (Walls == null)
        {
            Walls = new bool[PacmanGame.Columns][];
            for (int column = 0; column < PacmanGame.Columns; column++)
            {
                Walls[column] = new bool[PacmanGame.Rows];
                for (int row = 0; row < PacmanGame.Rows; row++)
                {
                    RaycastHit[] hits = PacmanGame.SweepTestCell(column, row, QueryTriggerInteraction.Ignore, false);
                    hits = MiniGame.FindHitsByType(hits, typeof(PacmanFrame));
                    Walls[column][row] = hits.Length > 0;
                }
            }
        }
        #endregion
    }

    void PrintFrame()
    {
        for (int row = PacmanGame.Rows - 1; row >= 0; row--)
        {
            string line = "Row " + row.ToString() + ": ";
            for (int column = 0; column < PacmanGame.Columns; column++)
            {
                line += (Walls[column][row] ? "[X]" : "[ ]");
            }
            Util.Log(line, gameObject);
        }
    }

    public bool IsValidCell(MiniGame.Cell cell)
    {
        return (cell == null ? false : IsValidCell(cell.Column, cell.Row));
    }

    public bool IsValidCell(int column, int row)
    {
        return column >= 0 &&
            column < Walls.Length &&
            row >= 0 &&
            row < Walls[column].Length;
    }

    public bool IsValidCellForActor(MiniGame.Cell cell)
    {
        return (cell == null ? false : IsValidCellForActor(cell.Column, cell.Row));
    }

    public bool IsValidCellForActor(int column, int row)
    {
        return IsValidCell(column, row) && !Walls[column][row];
    }

}
