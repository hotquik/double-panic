﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanWarpzoneFrom : MonoBehaviour
{
    GameObject m_to;

    public GameObject To { get => m_to; set => m_to = value; }

    void Awake ()
    {
        if (m_to == null)
            m_to = gameObject.transform.parent.Find("To").gameObject;
        if (m_to == null)
            Util.Log("'To' game object not found!", gameObject);
    }
}
