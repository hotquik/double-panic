﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PacmanActor : KinematicObject
{
    public class Movement
    {
        public MiniGame.Cell From;
        public MiniGame.Cell To;
        public MiniGameInput MiniGameInput;
        public Vector3 Direction;
        public float Distance;
        public float DistanceLeft;
        public bool lockToGrid = true;

        public Movement (MiniGame.Cell from, MiniGame.Cell to, MiniGameInput miniGameInput)
        {
            From = from;
            To = to;
            MiniGameInput = miniGameInput;
            Direction = To.Origin - From.Origin;
            DistanceLeft = Distance = Direction.magnitude;
            Direction = Direction.normalized;
        }

        public override string ToString()
        {
            return "PacmanActor.Movement: {From: " + From.ToString() + ", To: " + To.ToString() + ", MiniGameInput: " + MiniGameInput.ToString() 
                + ", Direction: " + Direction.ToString() + ", Distance: " + Distance.ToString() + ", DistanceLeft: " + DistanceLeft.ToString() + " }";
        }
    }
    public bool DoDebug;
    protected int column;
    protected int row;
    protected MiniGame.Cell currentCell = null;
    protected Movement currentMovement = null;
    protected bool lockedToGrid = false;

    protected MiniGameInputVectorMapping currentInput;
    protected MiniGameInputVectorMapping pendingInput;
    protected PacmanWarpzoneFrom pendingWarp;

    protected float actorMoveMagnitude = 1;
    protected float adjustedActorMoveMagnitude; // adjusted to game speed
    protected MiniGameInputVectorMapping[] inputDirectionMappings;
    protected MiniGameInputVectorMapping[] inputRotationMappings;
    protected Type[] collisionTypes = new Type[1] { typeof(PacmanFrame) };

    public PacmanGame PacmanGame { get; set; }
    public MiniGameInputVectorMapping PendingInput { get => pendingInput;
        set 
        {
            if (value == null 
                || value != currentInput) // ignore same direction input
            {
                pendingInput = value;
            }
        }
    }

    public PacmanActor()
    {
        #region Create input mappings
        inputDirectionMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.Left, MiniGame.Left),
            new MiniGameInputVectorMapping (MiniGameInput.Right, MiniGame.Right),
            new MiniGameInputVectorMapping (MiniGameInput.Up, MiniGame.Up),
            new MiniGameInputVectorMapping (MiniGameInput.Down, MiniGame.Down)
        };

        inputRotationMappings = new MiniGameInputVectorMapping[] { };
        #endregion
    }

    public override void Start()
    {
        base.Start();
        LockToGrid();
    }

    public virtual void Config(float gameSpeed)
    {
        adjustedActorMoveMagnitude = actorMoveMagnitude * gameSpeed;
    }

    public override void Update()
    {
        base.Update();

        bool move;

        if (PacmanGame.GamePlaying)
        {
            float magnitude = Math.Min(adjustedActorMoveMagnitude * Time.deltaTime, PacmanGame.CellSize);
            float magnitudeLeft = magnitude;
            #region Continue previous movement
            if (currentMovement != null)
            {
                Vector3 newLocalPosition = transform.localPosition + currentMovement.Direction * magnitudeLeft;

                if (
                    (newLocalPosition.x < currentMovement.To.Origin.x && currentMovement.To.Origin.x < currentMovement.From.Origin.x) ||
                    (currentMovement.From.Origin.x < currentMovement.To.Origin.x && currentMovement.To.Origin.x < newLocalPosition.x) ||
                    (newLocalPosition.y < currentMovement.To.Origin.y && currentMovement.To.Origin.y < currentMovement.From.Origin.y) ||
                    (currentMovement.From.Origin.y < currentMovement.To.Origin.y && currentMovement.To.Origin.y < newLocalPosition.y) 
                    )
                {
                    Vector3 partial = currentMovement.To.Origin - transform.localPosition;
                    FinishMovement();
                    magnitudeLeft -= partial.magnitude;
                }
                else
                {
                    ContinueMovement(magnitudeLeft);
                    magnitudeLeft = 0;
                }
            }
            #endregion
            #region Start new movement
            if (magnitudeLeft > 0 && lockedToGrid && PendingInput != null)
            {
                move = StartMovement(PendingInput, magnitudeLeft);
                if (move)
                {
                    Log("changing direction " + PendingInput.ToString());
                    currentInput = PendingInput;
                    PendingInput = null;
                    magnitudeLeft = 0;
                }                    
            }
            if (magnitudeLeft > 0 && lockedToGrid && currentInput != null)
            {
                move = StartMovement(currentInput, magnitudeLeft);
                if (move)
                {
                    Log("same direction " + currentInput.ToString());
                    magnitudeLeft = 0;
                }
                else
                {
                    Log("stopped at wall");
                    currentInput = null;
                }
            }
            #endregion
        }
    }

    protected void Log(string s)
    {
        if (DoDebug)
        {
            Util.Log(s, gameObject);
        }
    }

    private int LimitValue ( int value, int min, int max )
    {
        int result = Math.Max(value, min);
        result = Math.Min(result, max);
        return result;
    }

    public MiniGame.Cell NextCell(MiniGameInput miniGameInput)
    {
        int column2 = column;
        int row2 = row;
        switch (miniGameInput)
        {
            case MiniGameInput.Up:
                row2++;
                break;
            case MiniGameInput.Down:
                row2--;
                break;
            case MiniGameInput.Left:
                column2--;
                break;
            case MiniGameInput.Right:
                column2++;
                break;
        }
        return PacmanGame.GetCell(column2, row2);
    }

    public void LockToGrid()
    {
        Log("lockToGrid()");
        if (pendingWarp != null)
        {
            transform.localPosition = pendingWarp.To.transform.localPosition;
            pendingWarp = null;
        }

        float columnF = transform.localPosition.x / PacmanGame.CellSize;
        float rowF = transform.localPosition.y / PacmanGame.CellSize;
        column = (int)Math.Round(columnF);
        row = (int)Math.Round(rowF);
        column = LimitValue(column, 0, PacmanGame.Columns - 1);
        row = LimitValue(row, 0, PacmanGame.Rows - 1);
        currentCell = PacmanGame.Cells[column][row];
        currentMovement = null;
        Vector3 newLocalPosition = PacmanGame.CellOriginPosition(column, row);
        transform.localPosition = newLocalPosition;
        lockedToGrid = true;

    }

    public bool StartMovement(MiniGameInputVectorMapping mapping, float magnitude)
    {
        bool result = false;
        MiniGame.Cell nextCell = NextCell(mapping.MiniGameInput);
            
        bool collides = !PacmanGame.PacmanFrame.IsValidCellForActor(nextCell);

        if (!collides)
        {
            currentMovement = new Movement(currentCell, nextCell, mapping.MiniGameInput);
            currentCell = null;
            Move(mapping.Vector, magnitude);
            lockedToGrid = false;
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }

    public virtual void FinishMovement()
    {
        transform.localPosition = currentMovement.To.Origin;
        if (currentMovement.lockToGrid)
            LockToGrid();
    }

    public void ContinueMovement(float magnitude)
    {
        Move(currentMovement.Direction, magnitude);
        currentMovement.DistanceLeft -= magnitude;
    }

    public virtual bool TryMove(MiniGameInputVectorMapping mapping, float magnitude)
    {
        bool result = false;
        if (lockedToGrid)
        {
            MiniGame.Cell nextCell = NextCell(mapping.MiniGameInput);

            bool collides = !PacmanGame.PacmanFrame.IsValidCellForActor(nextCell);

            if (!collides)
            {
                Move(mapping.Vector, magnitude);
                result = true;
            }
            else
            {
                result = false;
            }
        }
        else
        {
            Move(mapping.Vector, magnitude);
            result = true;
        }
        return result;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Log("PacmanActor<" + name + ">.OnTriggerEnter(" + other.gameObject.name + ")");

        #region Collision with PacmanWarpzoneFrom
        PacmanWarpzoneFrom pacmanWarpzoneFrom = other.gameObject.GetComponent<PacmanWarpzoneFrom>();
        if (pacmanWarpzoneFrom != null)
        {
            pendingWarp = pacmanWarpzoneFrom;
        }
        #endregion
    }

}
