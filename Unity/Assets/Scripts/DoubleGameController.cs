﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoubleGameController : GameController
{
    public float GameAcceleration = 0.1f;
    public DoubleGameUI gui;
    public GameObject World;
    public MiniGame[] miniGames;
    public int lifes = 3;

    MiniGameHandler[] miniGameHandlers;
    KeyMapping[] leftKeyMappings;
    KeyMapping[] rightKeyMappings;

    int miniGameIndex = 0;
    bool gameOver = false;

    public override void Awake()
    {
        base.Awake();

        #region assign gui var
        if (gui == null)
        {
            gui = (DoubleGameUI)FindObjectOfType(typeof(DoubleGameUI));

            if (gui == null)
            {
                Debug.Log("GUI not found");
            }
        }
        #endregion
    }

    public override void Start()
    {
        base.Start();

        #region assign key mappings
        leftKeyMappings = new KeyMapping[]
        {
            new KeyMapping (KeyCode.W, MiniGameInput.Up),
            new KeyMapping (KeyCode.A, MiniGameInput.Left),
            new KeyMapping (KeyCode.S, MiniGameInput.Down),
            new KeyMapping (KeyCode.D, MiniGameInput.Right),
        };
        rightKeyMappings = new KeyMapping[]
        {
            new KeyMapping (KeyCode.UpArrow, MiniGameInput.Up),
            new KeyMapping (KeyCode.LeftArrow, MiniGameInput.Left),
            new KeyMapping (KeyCode.DownArrow, MiniGameInput.Down),
            new KeyMapping (KeyCode.RightArrow, MiniGameInput.Right),
        };
        #endregion

        #region initialize handlers
        miniGameHandlers = new[]
        {
             new MiniGameHandler(0, gui.LeftScoreBoard, gui.LeftMiniGameMessage, new Vector3(-11, 11, 0), leftKeyMappings), // LEFT
             new MiniGameHandler(1, gui.RightScoreBoard, gui.RightMiniGameMessage, new Vector3(11, 11, 0), rightKeyMappings) // RIGHT
        };
        #endregion

        #region create mini games
        for (int i = 0; i < miniGameHandlers.Length; i++)
        {
            NextMiniGame(miniGameHandlers[i]);
        }
        #endregion
    }


    public override void Update()
    {
        base.Update();

        if (!gameOver)
        {
            #region Check status and Update score, lifes
            int totalScore = 1;
            for (int i = 0; i < miniGameHandlers.Length; i++)
            {
                CheckMiniGameStatus(miniGameHandlers[i]);
                totalScore *= (miniGameHandlers[i].totalScore + miniGameHandlers[i].miniGame.Score);
            }

            gui.TotalScoreBoard.Score = totalScore;
            gui.LifesBoard.Score = lifes;
            #endregion

            #region Check general Game Over condition
            if (lifes <= 0)
            {
                for (int i = 0; i < miniGameHandlers.Length; i++)
                {
                    miniGameHandlers[i].miniGame.GameOver = true;
                }
                gui.OnGameOver();
                gameOver = true;
            }
            #endregion
        }
    }


    void CheckMiniGameStatus (MiniGameHandler miniGameHandler)
    {
        miniGameHandler.scoreBoard.Score = miniGameHandler.miniGame.Score;
        #region Check if miniGame has finished 
        if (miniGameHandler.active && 
            (miniGameHandler.miniGame.GameOver || miniGameHandler.miniGame.Victory))
        {
            miniGameHandler.active = false;
            miniGameHandler.totalScore += miniGameHandler.miniGame.Score;
            string message;              
            if (miniGameHandler.miniGame.GameOver) {
                lifes--;
                message = "Too bad!";
            }
            else
            {
                message = "Great!";
            }
            miniGameHandler.miniGameMessage.ShowMessage(message, miniGameHandler.MessageAnimationTime, OnMiniGameEnded, miniGameHandler);
        }
        #endregion
    }

    void OnMiniGameEnded(MiniGameHandler miniGameHandler)
    {
        #region If enough lifes, create a new miniGame
        if (lifes > 0)
        {
            Destroy(miniGameHandler.miniGame.gameObject);
            NextMiniGame(miniGameHandler);
        }
        #endregion
    }

    void NextMiniGame (MiniGameHandler miniGameHandler)
    {
        if (miniGames.Length == 0) return ;

        #region choose next prefab. it cant be a game in play
        MiniGame prefab = null; 
        int prefabIndex = -1;

        while (prefab == null)
        {
            bool miniGameInUse = false;
            for ( int i = 0; i < miniGameHandlers.Length && !miniGameInUse; i++ )
            {
                if (miniGameHandlers[i].prefabIndex == miniGameIndex)
                {
                    miniGameInUse = true;
                }
            }

            if (!miniGameInUse)
            {
                prefabIndex = miniGameIndex;
                prefab = miniGames[prefabIndex];
            }
            miniGameIndex = (miniGameIndex + 1) % miniGames.Length;
        }
        #endregion

        #region create minigame
        MiniGame miniGame = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        miniGame.transform.SetParent ( World.transform );
        miniGame.Config(this, GameSpeed);
        miniGame.transform.localPosition = miniGameHandler.localPosition;
        GameSpeed *= (1f + GameAcceleration);
        #endregion

        #region update handler info
        miniGameHandler.prefabIndex = prefabIndex;
        miniGameHandler.prefab = prefab;
        miniGameHandler.miniGame = miniGame;
        miniGameHandler.miniGameMessage.ShowMessage("Get Ready!", miniGameHandler.MessageAnimationTime, OnMiniGameCreated, miniGameHandler);
        #endregion

    }
    
    void OnMiniGameCreated(MiniGameHandler miniGameHandler)
    {
        #region Start the miniGame when its ready
        miniGameHandler.active = true;
        miniGameHandler.miniGame.StartGame();
        miniGameHandler.miniGameMessage.ShowMessage(miniGameHandler.miniGame.GameName + "!", miniGameHandler.MessageAnimationTime);
        #endregion
    }

    #region Mini Game Inputs
    public MiniGameHandler GetMiniGameHandler (MiniGame miniGame)
    {
        for (int i = 0; i < miniGameHandlers.Length; i++)
        {
            if (miniGameHandlers[i].miniGame == miniGame)
            {
                return miniGameHandlers[i];
            }
        }
        return null;
    }

    public override bool GetMiniGameInputDown(MiniGame minigame, MiniGameInput miniGameInput)
    {
        MiniGameHandler miniGameHandler = GetMiniGameHandler(minigame);
        if (miniGameHandler == null) return false;
        KeyMapping[] keyMappings = miniGameHandler.keyMappings;

        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKeyDown(keyMappings[i].KeyCode);
            }
        }
        return result;
    }

    public override bool GetMiniGameInputUp(MiniGame minigame, MiniGameInput miniGameInput)
    {
        MiniGameHandler miniGameHandler = GetMiniGameHandler(minigame);
        if (miniGameHandler == null) return false;
        KeyMapping[] keyMappings = miniGameHandler.keyMappings;

        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKeyUp(keyMappings[i].KeyCode);
            }
        }
        return result;
    }

    public override bool GetMiniGameInput(MiniGame minigame, MiniGameInput miniGameInput)
    {
        MiniGameHandler miniGameHandler = GetMiniGameHandler(minigame);
        if (miniGameHandler == null) return false;
        KeyMapping[] keyMappings = miniGameHandler.keyMappings;

        bool result = false;
        for (int i = 0; i < keyMappings.Length; i++)
        {
            if (keyMappings[i].MiniGameInput == miniGameInput)
            {
                result |= Input.GetKey(keyMappings[i].KeyCode);
            }
        }
        return result;
    }
    #endregion

    public GameObject InstantiateObject(Vector3 localPosition, GameObject prefab)
    {
        GameObject go = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        go.transform.SetParent (World.transform);
        go.transform.localPosition = localPosition;
        return go;
    }
}