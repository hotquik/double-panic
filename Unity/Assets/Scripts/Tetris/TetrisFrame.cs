﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TetrisFrame : MonoBehaviour
{
    public int blackMarginSize = 1;
    public int colorSquareSize = 8;
    public Color angle0 = new Color ( 0.333f,  0.666f,  0.0f );

    MeshRenderer meshRenderer;

    const float blackCellPercentage = 5;
    const int backMaterialIndex = 0;
    const int frontMaterialIndex = 1;
    const float minChannelValue = 0.1f;

    public TetrisGame TetrisGame { get; set; }

    static int colorColorPropertyId = Shader.PropertyToID("_MainTex");
    static int specColorPropertyId = Shader.PropertyToID("_SpecGlossMap");
    static int baseMapPropertyId = Shader.PropertyToID("_BaseMap");

    void Awake ()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

    void Start()
    {
        CreateTexture(22, 22);
    }

    void CreateTexture(int  columns, int rows)
    {
        int cellSize = blackMarginSize + colorSquareSize + blackMarginSize;
        int canvasWidth = columns * cellSize;
        int canvasHeight = rows * cellSize;
        int totalCells = columns * rows;

        var tex = new Texture2D(canvasWidth, canvasHeight, TextureFormat.RGB24, true);
        int bytesPerPixel = 3; // 3 bytes per pixel on RGB24 format
        var data = new byte[columns * rows * cellSize * cellSize * bytesPerPixel];

        #region Iterate on the grid to paint cells
        for (int row = 0; row < rows; row++)
        {
            int rowY = row * cellSize;
            int rowPos = rowY * canvasWidth;
            float rowN = (float)row / (float)rows;

            for (int column = 0; column < columns; column++)
            {
                int columnX = column * cellSize;
                int columnPos = columnX;
                float colN = (float)column / (float)columns;

                float rand = UnityEngine.Random.value * 100f; // randomly generate black cells

                #region Paint the cell when needed
                if (rand > blackCellPercentage)
                {
                    byte red = 0, green = 0, blue = 0;
                    red = getFrameChannelValue(angle0.r, colN, rowN);
                    green = getFrameChannelValue(angle0.g, colN, rowN);
                    blue = getFrameChannelValue(angle0.b, colN, rowN);

                    for (int x = blackMarginSize; x < blackMarginSize + colorSquareSize; x++)
                    {
                        for (int y = blackMarginSize; y < blackMarginSize + colorSquareSize; y++)
                        {
                            int globalY = y + rowY;
                            int globalX = x + columnX;
                            int pos = (globalY * canvasWidth + globalX) * bytesPerPixel;

                            data[pos + 0] = red;
                            data[pos + 1] = green;
                            data[pos + 2] = blue;
                        }
                    }
                }
                #endregion Paint the cell when needed
            }
        }
        #endregion

        tex.SetPixelData(data, 0, 0); // mip 0

        tex.filterMode = FilterMode.Point; // Disable antialiasing
        tex.Apply(updateMipmaps: false);

        /*
        meshRenderer.materials[frontMaterialIndex].SetTexture(colorColorPropertyId, tex);
        meshRenderer.materials[frontMaterialIndex].SetTexture(specColorPropertyId, tex);
        */
        meshRenderer.materials[frontMaterialIndex].SetTexture(baseMapPropertyId, tex);
    }
       
    byte getFrameChannelValue(float channelAngle0, float colN, float rowN)
    {
        float sinParameter = (channelAngle0 + colN) * (float)Math.PI * 2f;
        float val1 = ((float)Math.Sin(sinParameter) + 0.25f) * (minChannelValue + (1f - minChannelValue) * rowN);
        float val2 = clamp(val1, 0.0f, 1.0f);
        return (byte)( val2 * 255);
    }    

    float clamp(float val, float  min, float max)
    {
        return (val < min ? min : (val > max ? max : val));
    }
}
