﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisCube : MonoBehaviour
{
    public Rigidbody rigidBody;
    public Material cornerMaterial;
    public Material defaultMaterial;
    public Material alternativeMaterial;

    const int cornerMaterialIndex = 0;
    const int centerMaterialIndex = 1;

    MeshRenderer meshRenderer;

    public TetrisPiece TetrisPiece { get ; set ; }
    public TetrisCubesGroup TetrisCubesGroup { get; set; }
    public TetrisGame TetrisGame { get; set; }

    public bool InFullRow  { get; set; }
    public float TimeUntilDestroy { get; set; }

    float cubeBlinkInterval = 1;
    int blinkFrameInterval = 2, blinkFrameCounter = 0;
    
    void Awake()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        SetMaterial(defaultMaterial);
    }

    private void SetMaterial(Material material)
    {
        Material[] mats = meshRenderer.materials;
        mats[centerMaterialIndex] = material;
        meshRenderer.materials = mats;
    }

    public void SwapMaterials()
    {
        (defaultMaterial, alternativeMaterial) = (alternativeMaterial, defaultMaterial);

        SetMaterial(defaultMaterial);
    }

    public void Config(float gameSpeed)
    {
        cubeBlinkInterval = TetrisGame.AdjustedPieceMoveTimeInterval / 2;        
    }

    void Update()
    {
        if (InFullRow)
        {
            if (blinkFrameCounter == 0)
            {
                SwapMaterials();
            }
            blinkFrameCounter = (blinkFrameCounter + 1) % blinkFrameInterval;
        }
    }

    public void OnFullRow ()
    {
        InFullRow = true;
        TimeUntilDestroy = cubeBlinkInterval;

    }

    public void InvokeDestroy()
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        if (TetrisPiece != null)
        {
            TetrisPiece.OnTetrisCubeDestroy(this);
        }
    }

}
