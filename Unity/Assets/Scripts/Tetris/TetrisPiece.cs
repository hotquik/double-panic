﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class TetrisCubesGroup : List<TetrisCube>
{
}

public class TetrisPiece : MonoBehaviour
{

    public Material defaultMaterial;
    public Material alternativeMaterial;
    public bool Centered = false;
    public bool Active = false;
    
    List<Collider> collisions = new List<Collider>();

    const int startCubesCount = 4;
    int cubesCount = startCubesCount;
    List<TetrisCubesGroup> tetrisCubesGroups;
    
    public TetrisGame TetrisGame { get; set; }
    public long Id { get => id; set => id = value; }
    public bool IsComplete { get => cubesCount == startCubesCount; }

    static long idCounter = 0;
    long id;

    const float materialRandomizerPercentage = 30;

    static int colorColorPropertyId = Shader.PropertyToID("_Color");
    static int specColorPropertyId = Shader.PropertyToID("_SpecColor");
    static int baseColorPropertyId = Shader.PropertyToID("_BaseColor");

    void Awake()
    {
        id = idCounter;
        idCounter++;

        #region create the list of groups and the default group
        tetrisCubesGroups = new List<TetrisCubesGroup>();
        TetrisCubesGroup defaultGroup = new TetrisCubesGroup();
        tetrisCubesGroups.Add(defaultGroup);
        #endregion

        #region add all cubes to the default group
        TetrisCube[] cubes = GetComponentsInChildren<TetrisCube>();
        for (int i = 0; i < cubes.Length; i++)
        {
            TetrisCube tetrisCube = (TetrisCube)cubes[i];
            tetrisCube.TetrisPiece = this;
            tetrisCube.TetrisCubesGroup = defaultGroup;
            tetrisCube.TetrisGame = TetrisGame;
            tetrisCube.defaultMaterial = RandomizeMaterial(defaultMaterial, materialRandomizerPercentage, defaultMaterial.name + "(" + i.ToString()+ ")");
            tetrisCube.alternativeMaterial = alternativeMaterial;
            defaultGroup.Add(tetrisCube);
        }
        #endregion
    }

    public void Config(float gameSpeed)
    {
        for (int g = 0; g < tetrisCubesGroups.Count; g++)
        {
            TetrisCubesGroup group = tetrisCubesGroups[g];
            for (int i = 0; i < group.Count; i++)
            {
                TetrisCube tetrisCube = group[i];
                tetrisCube.Config(gameSpeed);
            }
        }
    }

    public bool TryMove (Vector3 direction, float magnitude)
    {
        bool anyGroupMoved = false;
        #region check if any group can be moved
        for ( int g = 0; g < tetrisCubesGroups.Count; g++)
        {
            TetrisCubesGroup group = tetrisCubesGroups[g];
            bool collides = SweepTest(group, direction, magnitude, QueryTriggerInteraction.Collide);
            #region if the group doesnt collide, it can be moved
            if (!collides)
            {
                #region if piece is complete, move the piece game object
                if (IsComplete)
                {
                    Move(direction, magnitude, transform);
                }
                #endregion
                #region if piece is incomplete, move all cubes 
                else
                {
                    for (int i = 0; i < group.Count; i++)
                    {
                        Move(direction, magnitude, group[i].transform);
                    }
                }
                #endregion
                anyGroupMoved = true;
            }
            #endregion
        }
        #endregion
        return anyGroupMoved;
    }
    void Move(Vector3 direction, float magnitude, Transform transf)
    {
        transf.position = transf.position + direction * magnitude;
    }

    bool SweepTest(TetrisCubesGroup group , Vector3 direction, float maxDistance = Mathf.Infinity, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal )
    {
        bool anyCubeCollides = false;

        for (int i = 0; i < group.Count ; i++)
        {
            TetrisCube tetrisCube = group[i];
            RaycastHit[] hits = tetrisCube.rigidBody.SweepTestAll(direction,  maxDistance, queryTriggerInteraction);

            for (int j = 0; j < hits.Length ; j++)
            {
                GameObject otherGameObject = hits[j].transform.gameObject;
                #region ignore hits against a TetrisPiece ( used for rotation )
                TetrisPiece otherTetrisPiece = otherGameObject.GetComponent<TetrisPiece>();
                if (otherTetrisPiece != null)
                {
                    continue;
                }
                #endregion
                #region ignore hits against a TetrisCube of this group
                TetrisCube otherTetrisCube = otherGameObject.GetComponent<TetrisCube>();
                if (otherTetrisCube != null && group.Contains(otherTetrisCube))
                {
                    continue;
                }
                #endregion

                // object does collide for move test !
                anyCubeCollides = true;
                break;
            }
            if (anyCubeCollides) break;
        }

        return anyCubeCollides;
    }

    public bool TryRotate(Vector3 direction, float magnitude)
    {
        if (collisions.Count > 0)
        {
            return false;
        }
        else
        {
            Rotate(direction, magnitude);
            return true;
        }
    }

    void Rotate(Vector3 direction, float magnitude)
    {
        transform.localEulerAngles = transform.localEulerAngles + direction * magnitude;
    }
       
    bool Contains (Collider collider)
    {
        TetrisCube tetrisCube = collider.gameObject.GetComponent<TetrisCube>();
        if (tetrisCube != null)
        {
            return Contains(tetrisCube);
        }
        return false;
    }

    bool Contains (TetrisCube tetrisCube)
    {
        return tetrisCube.TetrisPiece.Id == id;
    }

    // Detect rotation collisions to enable/disable rotation
    void OnTriggerEnter(Collider other)
    {
        #region ignore hits against a TetrisPiece ( used for rotation )
        TetrisPiece otherTetrisPiece = other.gameObject.GetComponent<TetrisPiece>();
        if (otherTetrisPiece != null)
        {
            return;
        }
        #endregion
        #region ignore hits against a TetrisCube of this TetrisPiece 
        TetrisCube otherTetrisCube = other.gameObject.GetComponent<TetrisCube>();
        if (otherTetrisCube != null && Contains(otherTetrisCube))
        {
            return;
        }
        #endregion
        #region ignore hits against a TetrisEntryPoint 
        TetrisEntryPoint tetrisEntryPoint = other.gameObject.GetComponent<TetrisEntryPoint>();
        if (tetrisEntryPoint != null)
        {
            return;
        }
        #endregion

        collisions.Add(other);

    }
    
    void OnTriggerExit(Collider other)
    {
        if ( collisions.Remove(other) )
        {
        }
    }

    public void OnTetrisCubeDestroy (TetrisCube tetrisCube)
    {
        #region split the group of cubes if necesary
        TetrisCubesGroup group = tetrisCube.TetrisCubesGroup;
        for ( int i = 0; i < group.Count; i ++)
        {
            if (tetrisCube == group[i])
            {
                #region if the cube is the first one or the last one, no split is needed
                if (i == 0 || i == group.Count - 1)
                {
                }
                #endregion
                #region do the split . the cubes are added to a new group and removed from the old one
                else
                {
                    i++;
                    TetrisCubesGroup newGroup = new TetrisCubesGroup();
                    for (  ; i < group.Count; i++)
                    {
                        newGroup.Add(group[i]);
                        group[i].TetrisCubesGroup = newGroup;
                        group.RemoveAt(i);
                    }
                    tetrisCubesGroups.Add(newGroup);
                }
                #endregion
                #region remove the destroyed cube from the group
                group.Remove(tetrisCube);
                if (group.Count == 0)
                {
                    tetrisCubesGroups.Remove(group);
                }
                #endregion
            }
        }
        #endregion

        #region notify parent game that a cube has been destroyed
        if (TetrisGame != null)
        {
            TetrisGame.OnTetrisCubeDestroy(tetrisCube);
        }
        #endregion

        #region check if piece has any cubes left
        cubesCount--;
        if (cubesCount == 0)
        {
            Destroy(gameObject);
        }
        #endregion
    }
    void OnDestroy()
    {
        if (TetrisGame != null)
        {
            TetrisGame.OnTetrisPieceDestroy(this);
        }
    }

    public void DebugPiece (string title = "")
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(title);
        sb.Append(gameObject.name);
        sb.Append(" Groups( " + tetrisCubesGroups.Count.ToString() + " ): {");
        for (int g = 0; g < tetrisCubesGroups.Count; g++)
        {
            if ( g > 0 ) sb.Append(" , ");
            TetrisCubesGroup group = tetrisCubesGroups[g];
            sb.Append(" { (" + group.Count.ToString() + ") " );
            for (int i = 0; i < group.Count; i++)
            {
                TetrisCube tetrisCube = group[i];
                if (i > 0) sb.Append(" , ");
                sb.Append(tetrisCube.name);

            }
            sb.Append(" } ");
        }
        sb.Append(" } ");
        Debug.Log(sb.ToString());
    }

    /// <summary>
    /// Create a new material similar to the parameter
    /// </summary>
    /// <param name="material"></param>
    /// <param name="percentage"></param>
    /// <param name="newName"></param>
    /// <returns></returns>
    private Material RandomizeMaterial(Material material, float percentage, string newName)
    {
        Material newMaterial = new Material(material);
        newMaterial.name = newName;

        /*
        Color color = newMaterial.GetColor(colorColorPropertyId);
        color = RandomizeColor(color, percentage);
        newMaterial.SetColor(colorColorPropertyId, color);

  
        color = newMaterial.GetColor(specColorPropertyId);
        color = RandomizeColor(color, percentage);
        newMaterial.SetColor(specColorPropertyId, color);
        */

        
        Color color = newMaterial.GetColor(baseColorPropertyId);
        color = RandomizeColor(color, percentage);
        newMaterial.SetColor(baseColorPropertyId, color);

        return newMaterial;
    }

    /// <summary>
    /// Change a color to a similar random one
    /// </summary>
    /// <param name="color"></param>
    /// <param name="percentage"></param>
    /// <returns></returns>
    private Color RandomizeColor(Color color, float percentage)
    {
        float maxDelta = 1 * (percentage / 100f);
        float halfMaxDelta = maxDelta / 2;
        color.r = clamp(color.r + maxDelta * UnityEngine.Random.value - halfMaxDelta, 0, 1);
        color.g = clamp(color.g + maxDelta * UnityEngine.Random.value - halfMaxDelta, 0, 1);
        color.b = clamp(color.b + maxDelta * UnityEngine.Random.value - halfMaxDelta, 0, 1);
        return color;
    }

    private float clamp(float val, float min, float max)
    {
        return (val < min ? min : (val > max ? max : val));
    }
}
