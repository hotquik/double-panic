﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisRowMarker : MonoBehaviour
{
    GameObject m_leftCube;
    GameObject m_rightCube;

    void Awake()
    {
        m_leftCube = gameObject.transform.Find("RightCube").gameObject;
        m_rightCube = gameObject.transform.Find("LeftCube").gameObject;

        if (m_leftCube == null) Debug.Log("TetrisRowMarker: RightCube not found");
        if (m_rightCube == null) Debug.Log("TetrisRowMarker: RightCube not found");
    }

    public List<TetrisCube> GetCubes (int layerMask)
    {
        List<TetrisCube> cubesList = new List<TetrisCube>();
        Vector3 v1 = m_leftCube.transform.position;
        Vector3 v2 = m_rightCube.transform.position;
        Vector3 direction = v2 - v1;
        RaycastHit[] hits = Physics.RaycastAll(v1, direction, direction.magnitude, layerMask, QueryTriggerInteraction.Collide);

        #region filter hits by object type
        for (int i = 0; i < hits.Length; i++)
        {
            TetrisCube tetrisCube = hits[i].transform.GetComponent<TetrisCube>();
            if (tetrisCube != null && !tetrisCube.InFullRow)
            {
                cubesList.Add(tetrisCube);
            }
        }
        #endregion

        return cubesList;
    }
}
