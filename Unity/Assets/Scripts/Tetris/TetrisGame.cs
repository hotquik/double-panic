﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TetrisGame : MiniGame
{
    public int FullRowPoints = 10;

    public GameObject[] TetrisPiecePrefabs = new GameObject[7];
    public TetrisEntryPoint TetrisEntryPoint;
    public float TimeUntilNextPieceMove { get => timeUntilNextPieceMove; set => timeUntilNextPieceMove = value; }
    public float AdjustedPieceMoveTimeInterval { get => adjustedPieceMoveTimeInterval; set => adjustedPieceMoveTimeInterval = value; }

    float timeUntilNextPieceMove;
    float pieceMoveMagnitude = 1;
    float pieceRotateMagnitude = 90;
    float pieceMoveTimeInterval = 1; // 1 sec default
    float adjustedPieceMoveTimeInterval; // adjusted to game speed

    List<TetrisCube> cubesToDestroy = new List<TetrisCube>();

    const int entryPointWidth = 4;
    const int entryPointHeight = 4;
    public int debugPieceIndex = -1;

    Vector3 centerFix;

    GameObject activePieceGO = null;
    TetrisPiece activePiece = null;
    MiniGameInput pendingInput = MiniGameInput.None;

    MiniGameInputVectorMapping[] inputMappings;
    TetrisRowMarker[] rowMarkers;
    TetrisFrame tetrisFrame;

    public override void Awake ()
    {
        columns = 10;
        base.Awake();
        LayerMask = 1 << 8;
        GameName = "Tetris";

        centerFix = new Vector3(CellSize / 2f, CellSize / 2f, 0);

        rowMarkers = GetComponentsInChildren<TetrisRowMarker>();
        tetrisFrame = GetComponentInChildren<TetrisFrame>();
        tetrisFrame.TetrisGame = this;
    }

    public override void Start()
    {
        base.Start();

        #region Create input mappings
        inputMappings = new MiniGameInputVectorMapping[]
        {
            new MiniGameInputVectorMapping (MiniGameInput.Up, Clockwise),
            new MiniGameInputVectorMapping (MiniGameInput.Left, Left),
            new MiniGameInputVectorMapping (MiniGameInput.Right, Right),
            new MiniGameInputVectorMapping (MiniGameInput.Down, Down)
        };
        #endregion
    }

    public override void Config(GameController GameController, float gameSpeed)
    {
        base.Config(GameController, gameSpeed);
        adjustedPieceMoveTimeInterval = pieceMoveTimeInterval / GameSpeed;
        timeUntilNextPieceMove = adjustedPieceMoveTimeInterval;
        if (activePiece != null)
        {
            activePiece.Config(gameSpeed);
        }
    }

    public override void Update()
    {
        base.Update();

        if (GamePlaying)            
        {
            #region no active piece in board, do clean up
            if (activePieceGO == null)
            {
                bool newFullLineFound = CheckFullLines();
                bool anyInactivePieceMoved = CheckInactivePiecesFall();
                bool anyCubePendingDestroy = CheckCubesToDestroy();

                if (!newFullLineFound && !anyInactivePieceMoved && !anyCubePendingDestroy)
                {
                    #region check if can be created
                    if (TetrisEntryPoint.IsEmpty)
                    {
                        activePieceGO = InstantiatePiece(/*TetrisEntryPoint.transform.position.x, TetrisEntryPoint.transform.position.y*/);
                        activePiece = activePieceGO.GetComponent<TetrisPiece>();
                        activePiece.TetrisGame = this;
                    }
                    else
                    {
                        OnGameOver();
                    }
                    #endregion
                }
            }
            #endregion
            #region active piece in board, move it
            else
            {
                #region get player input
                for ( int i = 0; i < inputMappings.Length; i++)
                {
                    if (GameController.GetMiniGameInputDown(this, inputMappings[i].MiniGameInput))
                    {
                        pendingInput = inputMappings[i].MiniGameInput;
                        break;
                    }
                }
                #endregion
                #region piece falls
                timeUntilNextPieceMove -= Time.deltaTime;
                if (timeUntilNextPieceMove < 0)
                {
                    timeUntilNextPieceMove += adjustedPieceMoveTimeInterval;
                    bool fall = activePiece.TryMove(Down, pieceMoveMagnitude);
                    #region piece cant fall anymore. becomes inactive
                    if (!fall)
                    {
                        activePiece.Active = false;
                        activePieceGO = null;
                        activePiece = null;
                        pendingInput = MiniGameInput.None;
                    }
                    #endregion
                }
                #endregion
                #region player can move the piece
                else
                {
                    if (pendingInput == MiniGameInput.None)
                    {
                        // do nothing
                    }
                    else if (pendingInput == MiniGameInput.Up)
                    {
                        bool rotate = activePiece.TryRotate(Clockwise, pieceRotateMagnitude);
                        pendingInput = MiniGameInput.None;
                    }
                    else if (pendingInput == MiniGameInput.Left || pendingInput == MiniGameInput.Right || pendingInput == MiniGameInput.Down)
                    {
                        Vector3 direction = Vector3.zero;
                        for (int i = 0; i < inputMappings.Length; i++)
                        {
                            if (pendingInput == inputMappings[i].MiniGameInput)
                                direction = inputMappings[i].Vector;
                        }
                        bool move = activePiece.TryMove(direction, pieceMoveMagnitude);
                        if (pendingInput != MiniGameInput.Down)
                        {
                            pendingInput = MiniGameInput.None;
                        }
                    }
                }
                #endregion
            }
            #endregion
        }
    }

    bool CheckFullLines()
    {
        #region check each row to verify it is full
        int fullRows = 0;
        for (int i = 0; i < rowMarkers.Length; i++)
        {
            TetrisRowMarker rowMarker = rowMarkers[i];
            List<TetrisCube> cubesList = rowMarker.GetCubes(LayerMask);

            #region check if the row is full
            if (cubesList.Count >= columns)
            {
                fullRows++;
                foreach (TetrisCube tetrisCube in cubesList)
                {
                    tetrisCube.OnFullRow();
                    cubesToDestroy.Add(tetrisCube);
                }
            }
            #endregion
        }
        #endregion
        #region score points
        if (fullRows > 0)
        {
            OnScorePoints(fullRows * fullRows * FullRowPoints);
        }
        #endregion
        return fullRows > 0;
    }    

    bool CheckInactivePiecesFall()
    {
        bool anyPieceMoved = false;
        TetrisPiece[] pieces = Objects.GetComponentsInChildren<TetrisPiece>();
        if (pieces.Length > 0)
        {
            for (int i = 0; i < pieces.Length; i++)
            {
                TetrisPiece tetrisPiece = pieces[i];
                bool fall = tetrisPiece.TryMove(Down, pieceMoveMagnitude);
                if (fall)
                {
                    anyPieceMoved = true;
                }
            }
        }
        return anyPieceMoved;
    }

    bool CheckCubesToDestroy()
    {
        bool anyCubeDestroyed = false;
        for (int i = cubesToDestroy.Count - 1;  i >= 0; i--)
        {
            TetrisCube tetrisCube = cubesToDestroy[i];
            tetrisCube.TimeUntilDestroy -= Time.deltaTime;
            if ( tetrisCube.TimeUntilDestroy <= 0)
            {
                tetrisCube.InvokeDestroy();
                cubesToDestroy.RemoveAt(i);
                anyCubeDestroyed = true;
            }
        }
        return cubesToDestroy.Count > 0 || anyCubeDestroyed;
    }

    GameObject InstantiatePiece()
    {
        int index = (debugPieceIndex > 0 ? debugPieceIndex : Random.Range(0, TetrisPiecePrefabs.Length));
        GameObject prefab = TetrisPiecePrefabs[index];
        TetrisPiece tp = prefab.GetComponent<TetrisPiece>();

        GameObject go = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        go.transform.parent = Objects.transform;

        go.transform.position = TetrisEntryPoint.transform.position ; // position;
        if (!tp.Centered) go.transform.position -= centerFix;
        tp = go.GetComponent<TetrisPiece>();
        tp.Active = true;
        return go;
    }

    public void OnTetrisCubeDestroy(TetrisCube tetrisCube)
    {
    }

    public void OnTetrisPieceDestroy(TetrisPiece tetrisPiece)
    {
    }
}
