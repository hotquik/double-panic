﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisEntryPoint : MonoBehaviour
{
    List<Collider> collisions = new List<Collider>();

    void OnTriggerEnter(Collider other)
    {
        collisions.Add(other);
    }

    void OnTriggerExit(Collider other)
    {
        if (collisions.Remove(other))
        {
        }
    }

    public bool IsEmpty
    {
        get { return collisions.Count == 0; }
    }
}
