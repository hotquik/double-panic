﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyMapping
{
    public KeyCode KeyCode { get ; set; }
    public MiniGameInput MiniGameInput  { get ; set; }

    public KeyMapping (KeyCode keyCode, MiniGameInput miniGameInput)
    {
        this.KeyCode = keyCode;
        this.MiniGameInput = miniGameInput;
    }
}