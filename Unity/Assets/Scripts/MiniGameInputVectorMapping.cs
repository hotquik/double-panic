﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameInputVectorMapping
{
    public MiniGameInput MiniGameInput { get; set; }
    public Vector3 Vector { get; set; }
    
    public MiniGameInputVectorMapping(MiniGameInput miniGameInput, Vector3 vector)
    {
        this.MiniGameInput = miniGameInput;
        this.Vector = vector;
    }
}